# BLUairspaceSDK

## Contents
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [BLUairspaceSDK](#bluairspacesdk)
- [Initialization](#initialization)
- [Markerbased](#markerbased)
- [Markerless](#markerless)
- [UI customization](#ui-customization)
- [App Store submission](#app-store-submission)
- [Resolving issues](#resolving-issues)


## Prerequisites
- iOS 11.1+
- Xcode 10.2+
- Swift 5+

## Installation

### CocoaPods

1. Install CocoaPods

[CocoaPods](https://cocoapods.org) is a dependency manager for Cocoa projects. For usage and installation instructions, visit their website.

2. Since BLUairspaceSDK uses git-lfs you will need to install it to clone/install BLUairspaceSDK through Cocoapods. Easiest way to install [git-lfs](https://git-lfs.github.com/) is using [brew](https://brew.sh/):

```bash
brew install git-lfs
git lfs install
```

3. Install SDK

To integrate BLUairspaceSDK into your Xcode project using CocoaPods, specify it in your `Podfile`:

```ruby
pod 'BLUairspaceSDK'
```

Then run:

```ruby
pod install
```

> In some cases installation process may encounter issues with downloading big files from Git LFS. In this case see [resolving issues section](#resolving-issues) for a solution.

### Manual

To use BLUairspace, you will need to add files from BLUExample/Libs folder into project:
* BLUs.framework
* WikitudeSDK.framework
* FirebaseCore.framework
* FirebaseAnalytics.framework

You can integrate BLUairspace SDK into your project manually by following these steps:
1. Right click the Xcode project folder (blue project icon) in the Project Navigator, then click Add Files to "Yourprojectname" menu item in the pop-up menu list. Navigate to the place in Finder that contains .framework files and add them into your Xcode project.

2. Then go to the app target's General configuration page and change Embed state for all four frameworks to "Embed & Sign" under the "Frameworks, Libraries, and Embedded Content" section.

#### Additional
The user's permissions are required in order to access the Camera on devices running iOS 10 or later. Add the **NSCameraUsageDescription** key in your Info.plist with a string that describes how your app will use this data. This key will appear as Privacy - Camera Usage Description in Xcode.

If you are targeting (you target) devices running iOS 11 or later, you will also need to add the **NSPhotoLibraryAddUsageDescription** key in your Info.plist. Use this key to define a string that describes how your app will use this data. By adding this key to your Info.plist, you will be able to request write-only access permission from the user. Without this permission, you will fail to save screenshots to camera roll.

We would also recommend you adding **NSCalendarsUsageDescription**, **NSPhotoLibraryUsageDescription** and **NSLocationWhenInUseUsageDescription**, if you want to fully enjoy all the benefits of our SDK. 

Eventually if everything you've done is correct, your Info.plist file should look like this:
```xml
...
<key>NSCameraUsageDescription</key>
<string>Camera access is required for augmented reality functionality.</string>
<key>NSPhotoLibraryAddUsageDescription</key>
<string>Photo additions access may be required for AR experience.</string>
<key>NSCalendarsUsageDescription</key>
<string>Access to calendar is needed to add events from AR</string>
<key>NSLocationWhenInUseUsageDescription</key>
<string>Access to your geo is needed to display markerbased AR depending on your location</string>
<key>NSPhotoLibraryUsageDescription</key>
<string>Photo access may be required for AR experience.</string>
...
```


## BLUairspaceSDK

BLUairspace SDK provides very simple interface that allows you to feel all the magic of Augumented Reality. BLUairspace SDK supports Markerbased AR and Markerless AR workflows and all the functionality of the BLUairspace platform.

**Markerbased AR** gives you an opportunity to detect, track and build entertaining experiences around a marker image.

With the help of **Markerless AR** you can place the object on the surface and it will be shown as if it's in the physical location. It can be moved, resized and interacted with from all angles.

## Initialization

Before we start please visit our [BLUairspace](https://www.realityblu.com) site to obtain unique RealityBLU license key linked to bundle identifier of your application.

Using either Markerbased or Markerless AR functionality of SDK, requires an initialization procedure to make basic SDK preparations for AR experiences. To do this just add the following code to your inside your AppDelegate `application(_:didFinishLaunchingWithOptions:)` method:

```swift
BLU.initialize("YOUR_LICENSE_KEY_HERE")
```
This method will send initialization request to BLUs server and obtain all the needed data. It also has an overload with completion handler to understand whether installation process was successful.

> The initialization method and other functions are available globally in any file with `import BLUs`


## Markerbased

Markerbased API has two main features:
1. Opening scanning controller
2. Downloading marker images

#### Scanning controller
By calling this API you can create a new screen that is needed to scan markers and display experiences after the marker is detected.
To create scanning controller and prepare it for launch, you need to call `prepareMarkerbased` function, that receives `PreparationCallback` handler consisting of initialize `BLUARController` object in case of success or `BLUError` object otherwise.

```swift
BLU.prepareMarkerbased { (controller, error) in
    if let _error = error {
        fatalError("Markerbase error: \(_error.description)")
    } else {
        //  At this stage we confident about controller 
        //  being initialized, so using force unwrap here
        present(controller!, animated: true)
    }
}
```

For proofing mode you will need to send as well `MarkerbasedOptions` structure with the needed preferences: 

```swift
let options = MarkerbasedOptions(enableProofing: true)
```

After that user can start pointing the camera at the marker. When the marker is recognized, the experience will be downloaded and shown.


#### Marker images

BLUairspace platform allows to upload markers and associate them with your application. This is sufficient for letting your end-users download and print markers on their own. Please note that the list of downloadable markers is configured separately from the markers you use in your experiences. 
The object that represents such an entity called `MarkerbasedMarker`.

You can get the list of configured markers using BLUDataHelper:
```swift
BLUDataHelper.getMarkerbasedMarkers { (markers) in 
    print("Markerbase scan targets list: \n\(markers)")
}
```

## Markerless

Markerless API allows you to upload your own models and create groups with them, that allows user find what they need to see. 

For handy work within this structure BLU SDK provides objects of two types: `MarkerlessGroup` and `MarkerlessExperience`. One `MarkerlessGroup` could contain many `MarkerlessExperience`.

BLUDataHelper is object responsible for managing of all the BLUs data models.
To get the list of formed groups, you'll need to make the following method call:

```swift
BLUDataHelper.getMarkerlessGroups { (groups) in
    print("Markerless groups list: \n\(groups)")
}
```

After that you could obtain `MarkerlessExperience` list out of selected group simply sending `groupId` parameter into `getMarkerlessExperiences` static function: 

```swift
BLUDataHelper.getMarkerlessExperiences(groupId: groupId) { (experiences) in
    print("Markerless experiences: \n\(experiences)")
}
```

You can also Markerless API helps you to download and initialize user selected experiences as well as prepare augmented reality camera screen to appear. To achieve this simply use `prepareMarkerless` method sending collection of `MarkerlessExperience` objects:

```swift
BLU.prepareMarkerless(experiences) { (controller, error) in
    if let _error = error {
        fatalError("Markerless error: \(_error.description)")
    } else {
        present(controller!, animated: true)
    }
}
```
Alternatively if you know `MarkerlessExperience` identifier, you have an ability to download and present it by calling the following method:

```swift
BLU.prepareMarkerlessExperience(withId: experienceId) { (controller, error) in
    if let _error = error {
        fatalError("Markerless error: \(_error.description)")
    } else {
        present(controller!, animated: true)
    }
```

> You can find this identifier on item details page visiting [BLUairspace](https://bluairspace.realityblu.com) site. 
> <br> 
> XRBuilder -> Markerless AR -> your folder -> your subfolder -> your item.

## UI customization

You can customize UI components such as images of scanning and icons of buttons by adding `BLUcustomization` folder into project. We would personally recommend you adding this folder to project directory using Finder.app and then add it to XCode as a reference folder, by selecting `Create folder references` instead of `Create groups` option in Add Files dialog window. In this case you could skip next step.

Go to your project Build Phases pane by highlighting the XCode project file in your solution explorer and on the right hand side, select `Build Phases`. You'll see that one of the sections you can expand is `Copy Bundle Resources`. Open that list and include `BLUcustomization` folder in that list.

`BLUcustomization` has to satisfy one specific trait: it requires `customization.json` file inside, desribing folder structure for the image and font resources.

Basic json file could look as follows:
```javascript
{
    "markerbased": {
        "scanning-spinner": "/markerbased/scanning-spinner.png",
        "scanning-spinner-text-svg": "/markerbased/scanning-spinner.svg",
        "loading-spinner": "/markerbased/loading-spinner.png",
        "loading-spinner-frames": 48,
        "camera-switch": "/markerbased/camera-switch.png",
        "lock-screen-on": "/markerbased/lock-screen-on.png",
        "lock-screen-off": "/markerbased/lock-screen-off.png",
        "qr-button": "/markerbased/qr-button.png"
    },
    "common": {
        "back-button": "/common/back-button.png",
        "snapshot": "/common/snapshot.png",
        "flight-off": "/common/flight-off.png",
        "flight-on": "/common/flight-on.png"
    }
}
```

> You can find a great example of `BLUcustomization` folder inside example app project


## App Store submission

BLUs.framework and WikitudeSDK.framework contain a fat binary with simulator (i386/x86_64) and device (ARM) slices, any application that attempts to embed that framework will be rejected from App Store submission. To workaround this you have to use a shell script that removes the simulator architectures from the .framework. 

To run this script simply copy the following snippet into a new `Run Script Phase` text field. Please make sure that the `Run Script Phase` is positioned right after the Embed Frameworks build phase (You can reorder individual build phases).

```bash
APP_PATH="\${TARGET_BUILD_DIR}/\${WRAPPER_NAME}"
# This script loops through the frameworks embedded in the application and
# removes unused architectures.
find "$APP_PATH" -name '*.framework' -type d | while read -r FRAMEWORK
do
    FRAMEWORK_EXECUTABLE_NAME=$(defaults read "$FRAMEWORK/Info.plist" CFBundleExecutable)
    FRAMEWORK_EXECUTABLE_PATH="$FRAMEWORK/$FRAMEWORK_EXECUTABLE_NAME"
    echo "Executable is $FRAMEWORK_EXECUTABLE_PATH"

    EXTRACTED_ARCHS=()

    for ARCH in $ARCHS
    do
        echo "Extracting $ARCH from $FRAMEWORK_EXECUTABLE_NAME"
        lipo -extract "$ARCH" "$FRAMEWORK_EXECUTABLE_PATH" -o "$FRAMEWORK_EXECUTABLE_PATH-$ARCH" 
        EXTRACTED_ARCHS+=("$FRAMEWORK_EXECUTABLE_PATH-$ARCH")
    done

    echo "Merging extracted architectures: \${ARCHS}"
    lipo -o "$FRAMEWORK_EXECUTABLE_PATH-merged" -create "\${EXTRACTED_ARCHS[@]}"
    rm "\${EXTRACTED_ARCHS[@]}"

    echo "Replacing original executable with thinned version"
    rm "$FRAMEWORK_EXECUTABLE_PATH"
    mv "$FRAMEWORK_EXECUTABLE_PATH-merged" "$FRAMEWORK_EXECUTABLE_PATH"
done
```

> Addidtionaly, please, remove all the *.sh files from framework contents, if it has any. 


## Resolving issues

**Error**: In some cases CocoaPods download not full files from Git LFS but only meta infomation. Files can be checked in this location:
`~/Library/Caches/CocoaPods/Pods/Release/BLUairspaceSDK/`

**Reason**: Caching issues with `Git LFS` and `CocoaPods`.

**Solution**: Possible solution to this issue is:
- running `pod cache clean BLUairspaceSDK` command
- remove wrong `Pods` folder from the root of the project `rm -rf Pods/`
- run `pod install` once again to download correct files
