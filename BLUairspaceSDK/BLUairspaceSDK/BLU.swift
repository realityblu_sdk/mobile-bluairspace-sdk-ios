//
//  BLU.swift
//  BLUairspaceSDK
//
//  Created by Jegor on 12/19/19.
//  Copyright © 2019 Jegor. All rights reserved.
//

import Foundation
import BLUs

public class BLU {
    /** Аn initialization procedure to make basic SDK preparations for AR experiences.
     - parameter licenseKey: RealityBLU license key, registered for your bundle id
     - parameter completion: Closure to handle initialization completion and errors
     */
    public static func initialize(_ licenseKey: String, completion: TaskCallback? = nil) {
        BLUs.BLU.initialize(licenseKey, completion: completion)
    }
    
    /** Create Markerbased AR scanning controller and prepare it for launch
     - parameter completion: Closure to handle Markerbased AR preparation procedure result
     */
    public static func prepareMarkerbased(_ completion: @escaping PreparationCallback) {
        BLUs.BLU.prepareMarkerbased(completion)
    }
    
    /** Create scanning controller using MarkerbasedOptions and prepare it for launch
     - parameter options: `MarkerbasedOptions` object to configure AR scanning controller
     - parameter completion: Closure to handle Markerbased AR preparation procedure result
     */
    public static func prepareMarkerbased(withOptions options: MarkerbasedOptions, completion: @escaping PreparationCallback) {
        BLUs.BLU.prepareMarkerbased(withOptions: options, completion: completion)
    }
    
    /** Starts downloading Markerless AR experiences and prepare Markerless AR ViewController for starting when download completes. BLU sdk can support up to 5 different markerless experiences at a time.
     - parameter experiences: Array of markerless experiences. Max length is 5
     - parameter progress: Closure for handling download progress event (e.g show progress dialog)
     - parameter completion: Closure to handle Markerbased AR preparation procedure result
     */
    public static func prepareMarkerlessExperiences(_ experiences: [MarkerlessExperience],
                                                    withProgress progress: ((Float) -> Void)?,
                                                    completion: @escaping PreparationCallback) {
        BLUs.BLU.prepareMarkerlessExperiences(experiences, withProgress: progress, completion: completion)
    }
    
    /** Starts downloading Markerless AR experience with identifier and prepare BLUController for starting when download completes.
     - parameter identifier: Experience to download identifier
     - parameter progress: Closure for handling download progress event (e.g show progress dialog)
     - parameter completion: Closure to handle Markerbased AR preparation procedure result
     */
    public static func prepareMarkerlessExperience(withId identifier: Int,
                                                   andProgress progress: ((Float) -> Void)?,
                                                   completion: @escaping PreparationCallback) {
        BLUs.BLU.prepareMarkerlessExperience(withId: identifier, andProgress: progress, completion: completion)
    }
    
    /** Start downloading Markerless experiences and prepares BLUController to appear after download complete. BLU sdk can support up to 5 different markerless experiences at a time.
     - parameter experiences: Array of markerless experiences. Max length is 5
     - parameter completion: Closure to handle Markerbased AR preparation procedure result
     */
    public static func prepareMarkerlessExperiences(_ experiences: [MarkerlessExperience],
                                                    completion: @escaping PreparationCallback) {
        prepareMarkerlessExperiences(experiences, withProgress: nil, completion: completion)
    }
    
    /** Start downloading Markerless experience with identifier and prepares BLUController to appear after download complete.
     - parameter identifier: Experience to download identifier
     - parameter completion: Closure to handle Markerbased AR preparation procedure result
     */
    public static func prepareMarkerlessExperience(withId identifier: Int,
                                                   completion: @escaping PreparationCallback) {
        prepareMarkerlessExperience(withId: identifier, andProgress: nil, completion: completion)
    }
}

public class BLUDataHelper {
    /** Requests the list of markerbased markers
     - parameter completion: Closure containing markers list
     */
    public static func getMarkerbasedMarkers(completion: @escaping (_ resultData: [MarkerbasedMarker]) -> Void) {
        BLUs.BLUDataHelper.getMarkerbasedMarkers(completion: completion)
    }
    
    /** Requests the list of markerless groups
     - parameter completion: Closure for handling markerless groups
     */
    public static func getMarkerlessGroups(_ completion: @escaping (_ resultData: [MarkerlessGroup]) -> Void) {
        BLUs.BLUDataHelper.getMarkerlessGroups(completion)
    }
    
    /** Requests the list of markerless experiences by group
     - parameter groupId: Selected markerless group identifier
     - parameter completion: Closure for handling markerless experiences list response
     */
    public static func getMarkerlessExperiences(groupId: Int, completion: @escaping (_ resultData: [MarkerlessExperience]) -> Void) {
        BLUs.BLUDataHelper.getMarkerlessExperiences(groupId: groupId, completion: completion)
    }
}
