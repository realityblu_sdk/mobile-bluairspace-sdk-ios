//
//  PlistManager.swift
//  BLUs
//
//  Created by Jegor on 11/29/19.
//  Copyright © 2019 Den Alex. All rights reserved.
//

import Foundation

@objc public class PlistManager: NSObject {
    static public var plistFileName: String?
    
    @objc public class func hasProperty(property: String) -> Bool {
        let _plistFileName = plistFileName ?? "Info"
        guard let path = Bundle.main.path(forResource: _plistFileName, ofType: "plist") else { return false }
        if let plistProperties = NSDictionary(contentsOfFile: path) {
            return (plistProperties[property] != nil)
        }
        return false
    }
}
