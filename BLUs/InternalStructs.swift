
import Foundation

struct BtnCalendarAction: Decodable {
    public let title: String?
    public let startDate: String?
    public let endDate: String?
    public let description: String?
}

