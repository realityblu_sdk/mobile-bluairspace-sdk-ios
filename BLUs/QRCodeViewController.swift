//
//  QRCodeViewController.swift
//  AppWareFrame
//
//  Created by Gleb on 5/13/19.
//  Copyright © 2019 RealityBlu. All rights reserved.
//

import AVFoundation
import UIKit

internal protocol QRScannerDelegate: class {
    func onQrScanned(code: String)
    func notifyScanCancelled()
    func onScanCancelled()
}

internal class QRCodeViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, ProofingControllerDelegate, BLUARController {
    
    var options: MarkerbasedOptions?
    
    var captureSession = AVCaptureSession()
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?
    
    var arController: ARBaseViewController = ARBaseViewController.instantiate()
    
    weak var delegate: QRScannerDelegate?
    
    @IBOutlet weak var contentView: UIView?
    @IBOutlet weak var leftView: UIView?
    @IBOutlet var scanFrame: UIImageView?
    @IBOutlet var safeAreaViews: [UIView]!
    @IBOutlet var safeAreaSideViews: [UIView]!
    
    
    deinit {
        L.log(message: "deinit QR controller", event: .info)
    }
    
    @IBAction func back(_ sender: Any) {
        cancelScan()
    }
    
    class func instantiate() -> QRCodeViewController {
        let storyboard = UIStoryboard(name: "BLUControllers", bundle: Bundle(for: QRCodeViewController.self))
        return storyboard.instantiateViewController(withIdentifier: "qrController") as! QRCodeViewController
    }

    override open func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        initQRCodeScanner()
        setupInterface()
    }
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        captureSession.startRunning()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        captureSession.stopRunning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    open func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObject.ObjectType.qr {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                let str = metadataObj.stringValue?.data(using: .utf8)
                if (try? JSONSerialization.jsonObject(with: str!, options: [])) != nil {
                    captureSession.stopRunning()
                    showARProofingController(withCode: metadataObj.stringValue!)
//                    delegate?.onQrScanned(code: metadataObj.stringValue!)
                }
            }
        }
    }
    
    internal func setupInterface() {
        self.scanFrame?.layer.borderColor = UIColor.white.cgColor
        self.scanFrame?.layer.borderWidth = 2.0
    }
    
    internal func initQRCodeScanner() {
        var deviceDiscoverySession: AVCaptureDevice.DiscoverySession?
        
        deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera, .builtInWideAngleCamera], mediaType: AVMediaType.video, position: .back)
        
        
        guard let captureDevice = deviceDiscoverySession!.devices.first else {
            print("Failed to get the camera device")
            cancelScan()
            return
        }
        do {
            // let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
            
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Set the input device on the capture session.
            captureSession.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            videoPreviewLayer?.masksToBounds = true
            videoPreviewLayer?.frame = CGRect(x: 0.0, y: 0.0, width: 100.0, height: 100.0) //contentView!.frame  // view.layer.bounds
            view.layer.addSublayer(videoPreviewLayer!)
            captureSession.startRunning()
            
            if let _content = contentView {
                view.bringSubview(toFront: _content)
            }
            safeAreaViews.forEach({ view.bringSubview(toFront: $0) })
//            safeAreaSideViews.forEach({ view.bringSubview(toFront: $0) })
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            self.navigationController?.popViewController(animated: false)
            print("Failed to get the camera device input")
            print(error.localizedDescription)
            return
        }
    }
    
    func startCaptureSession() {
        self.view.clipsToBounds = true
        captureSession.startRunning()
    }
    
    func showARProofingController(withCode code: String) {
        arController.proofingDelegate = self
        self.view.clipsToBounds = false
        arController.isProofing = true
        self.presentModal(arController) { [weak self] in
            self?.arController.enableProofing(withCode: code)
        }
    }
    
    open func cancelScan() {
        if self.isModal {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }

    private func updatePreviewLayer(layer: AVCaptureConnection, orientation: AVCaptureVideoOrientation) {
        layer.videoOrientation = orientation
        videoPreviewLayer?.frame = self.view.bounds
    }
    
    open override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let connection =  self.videoPreviewLayer?.connection {
            let currentDevice: UIDevice = UIDevice.current
            let orientation: UIDeviceOrientation = currentDevice.orientation
            let previewLayerConnection : AVCaptureConnection = connection
            if previewLayerConnection.isVideoOrientationSupported {
                
            switch (orientation) {
                case .portrait:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                    break
                case .landscapeRight:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeLeft)
                    break
                case .landscapeLeft:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeRight)
                    break
                case .portraitUpsideDown:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portraitUpsideDown)
                    break
                default:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                    break
                }
            }
        }
    }
}
