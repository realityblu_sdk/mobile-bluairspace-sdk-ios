
import Foundation

public enum UserIndustry: String {
    case unspecified
    case Healthcare
    case Finance
    case Marketing
    case Manufacturing
    case Entertainment
    case Sports
    case Other
    
    public func string() -> String {
        if self == UserIndustry.unspecified {
            return "Industry"
        } else {
            return self.rawValue
        }
    }
    
}

public enum UserMusic: String {
    case unspecified
    case Alternative
    case Classical
    case Country
    case RnB
    case ClassicRock
    case Jazz
    
    public func string() -> String {
        if self == UserMusic.unspecified {
            return "Music Preference"
        } else if self == UserMusic.RnB {
            return "R&B"
        } else if self == UserMusic.ClassicRock {
            return "Classic Rock"
        } else {
            return self.rawValue
        }
    }
}

enum AppConfiguration {
    case Debug
    case TestFlight
    case AppStore
}
