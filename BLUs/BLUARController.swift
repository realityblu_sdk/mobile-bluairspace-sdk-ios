//
//  BLUARController.swift
//  BLUs
//
//  Created by jegor on 11/4/19.
//  Copyright © 2019 Den Alex. All rights reserved.
//

import Foundation

public protocol BLUARController where Self: UIViewController  {
    var options: MarkerbasedOptions? { get set }
}
