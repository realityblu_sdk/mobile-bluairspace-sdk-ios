//
//  WTAugmentedRealityViewController+PluginLoading.m
//  SDKExamples
//
//  Created by Andreas Schacherbauer on 27/07/15.
//  Copyright (c) 2015 Wikitude. All rights reserved.
//

#import "WTAugmentedRealityViewController+PluginLoading.h"

#include <memory>
#import <objc/runtime.h>

#import <WikitudeSDK/WTArchitectView+Plugins.h>

#import "WTAugmentedRealityExperience.h"
//#import "BarcodePlugin.h"
//#import "WTFaceDetectionPluginWrapper.h"
//#import "SimpleYUVInputPlugin.hpp"
//#import "WTSimpleYUVInputCamera.h"
//#import "YUVFrameInputPlugin.hpp"
//#import "WTYUVInputCamera.h"
//#import "MarkerTrackingPlugin.h"

//static const char *kWTAugmentedRealityViewController_AssociatedFaceDetectionPluginWrapperKey = "kWTARVCAFDPWK";
//static const char *kWTAugmentedRealityViewController_AssociatedYUVInputCameraWrapperKey = "kWTARVCAYUVICWK";
//static const char *kWTAugmentedRealityViewController_AssociatedSimpleYUVInputCameraWrapperKey = "kWTARVCASYUVICWK";

NSString * const kWTPluginIdentifier_BarcodePlugin = @"com.wikitude.plugin.barcode";
NSString * const kWTPluginIdentifier_FacedetectionPlugin = @"com.wikitude.plugin.face_detection";
NSString * const kWTPluginIdentifier_SimpleInputPlugin = @"com.wikitude.plugin.simple_input_plugin";
NSString * const kWTPluginIdentifier_CustomCameraPlugin = @"com.wikitude.plugin.customcamera";
NSString * const kWTPluginIdentifier_MarkerTrackingPlugin = @"com.wikitude.plugin.markertracking";


@implementation WTAugmentedRealityViewController (PluginLoading)

- (void)loadNamedPlugin:(NSString *)pluginName forAugmentedRealityExperience:(WTAugmentedRealityExperience *)augmentedRealityExperience
{
    if ( [pluginName isEqualToString:kWTPluginIdentifier_BarcodePlugin] )
    {
    }
    else if ( [pluginName isEqualToString:kWTPluginIdentifier_FacedetectionPlugin] )
    {
//        NSString *exampleBasePath = [[[[augmentedRealityExperience URL] URLByDeletingLastPathComponent] absoluteString] stringByReplacingOccurrencesOfString:[[[NSBundle mainBundle] bundleURL] absoluteString] withString:@""];
//        NSString *databasePath = [[NSBundle mainBundle] pathForResource:@"high_database" ofType:@"xml" inDirectory:[exampleBasePath stringByAppendingPathComponent:@"assets"]];
//        WTFaceDetectionPluginWrapper *faceDetectionPluginWrapper = [[WTFaceDetectionPluginWrapper alloc] initWithDatabasePath:databasePath];
//        objc_setAssociatedObject(self, kWTAugmentedRealityViewController_AssociatedFaceDetectionPluginWrapperKey, faceDetectionPluginWrapper, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    else if ( [pluginName isEqualToString:kWTPluginIdentifier_SimpleInputPlugin] )
    {
//        WTSimpleYUVInputCamera *simpleInputCamera = [[WTSimpleYUVInputCamera alloc] init];
//        objc_setAssociatedObject(self, kWTAugmentedRealityViewController_AssociatedSimpleYUVInputCameraWrapperKey, simpleInputCamera, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    else if ( [pluginName isEqualToString:kWTPluginIdentifier_CustomCameraPlugin] )
    {
//        WTYUVInputCamera *yuvInputCamera = [[WTYUVInputCamera alloc] init];
//        objc_setAssociatedObject(self, kWTAugmentedRealityViewController_AssociatedYUVInputCameraWrapperKey, yuvInputCamera, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    else if ( [pluginName isEqualToString:kWTPluginIdentifier_MarkerTrackingPlugin] )
    {
        /* Intentionally Left Blank */
    }
}

- (void)startNamedPlugin:(NSString *)pluginName
{
    if ( [pluginName isEqualToString:kWTPluginIdentifier_BarcodePlugin] )
    {
//        NSError *pluginRegistrationError = nil;
//        BOOL pluginRegistered = [self.architectView registerPlugin:std::make_shared<BarcodePlugin>([kWTPluginIdentifier_BarcodePlugin cStringUsingEncoding:NSUTF8StringEncoding], 640, 480) error:&pluginRegistrationError];
//        if ( !pluginRegistered )
//        {
//            NSLog(@"Unable to register plugin '%@'. Reason: %@", pluginName, [pluginRegistrationError localizedDescription]);
//        }
    }
    else if ( [pluginName isEqualToString:kWTPluginIdentifier_FacedetectionPlugin] )
    {
//        WTFaceDetectionPluginWrapper *faceDetectionPluginWrapper = objc_getAssociatedObject(self, kWTAugmentedRealityViewController_AssociatedFaceDetectionPluginWrapperKey);
//        if ( faceDetectionPluginWrapper )
//        {
//            [faceDetectionPluginWrapper start];
//
//            NSError *pluginRegistrationError = nil;
//            BOOL pluginRegistered = [self.architectView registerPlugin:faceDetectionPluginWrapper.faceDetectionPlugin error:&pluginRegistrationError];
//            if ( !pluginRegistered )
//            {
//                NSLog(@"Unable to register plugin '%@'. Reason: %@", pluginName, [pluginRegistrationError localizedDescription]);
//            }
//        }
    }
    else if ( [pluginName isEqualToString:kWTPluginIdentifier_SimpleInputPlugin] )
    {
//        WTSimpleYUVInputCamera *simpleInputCamera = objc_getAssociatedObject(self, kWTAugmentedRealityViewController_AssociatedSimpleYUVInputCameraWrapperKey);
//        if ( simpleInputCamera )
//        {
//            NSError *pluginRegistrationError = nil;
//            BOOL pluginRegistered = [self.architectView registerPlugin:[simpleInputCamera cameraInputPlugin] error:&pluginRegistrationError];
//            if ( !pluginRegistered )
//            {
//                NSLog(@"Unable to register plugin '%@'. Reason: %@", pluginName, [pluginRegistrationError localizedDescription]);
//            }
//        }
    }
//    else if ( [pluginName isEqualToString:kWTPluginIdentifier_CustomCameraPlugin] )
//    {
//        WTYUVInputCamera *yuvInputCamera = objc_getAssociatedObject(self, kWTAugmentedRealityViewController_AssociatedYUVInputCameraWrapperKey);
//        if (yuvInputCamera)
//        {
//            NSError *pluginRegistrationError = nil;
//            BOOL pluginRegistered = [self.architectView registerPlugin:[yuvInputCamera cameraInputPlugin] error:&pluginRegistrationError];
//            if ( !pluginRegistered )
//            {
//                NSLog(@"Unable to register plugin '%@'. Reason: %@", pluginName, [pluginRegistrationError localizedDescription]);
//            }
//        }
//    }
    else if ( [pluginName isEqualToString:kWTPluginIdentifier_MarkerTrackingPlugin] )
    {
//        NSError *pluginRegistrationError = nil;
//        BOOL pluginRegistered = [self.architectView registerPlugin:std::make_shared<MarkerTrackingPlugin>() error:&pluginRegistrationError];
//        if ( !pluginRegistered )
//        {
//            NSLog(@"Unable to register plugin '%@'. Reason: %@", pluginName, [pluginRegistrationError localizedDescription]);
//        }
    }
}

- (void)stopNamedPlugin:(NSString *)pluginName
{
    if ( [pluginName isEqualToString:kWTPluginIdentifier_BarcodePlugin] )
    {
//        [self.architectView removeNamedPlugin:kWTPluginIdentifier_BarcodePlugin];
//    }
//    else if ( [pluginName isEqualToString:kWTPluginIdentifier_FacedetectionPlugin] )
//    {
//        WTFaceDetectionPluginWrapper *faceDetectionPluginWrapper = objc_getAssociatedObject(self, kWTAugmentedRealityViewController_AssociatedFaceDetectionPluginWrapperKey);
//        [faceDetectionPluginWrapper stop];
//        [self.architectView removePlugin:faceDetectionPluginWrapper.faceDetectionPlugin];
//    }
//    else if ( [pluginName isEqualToString:kWTPluginIdentifier_SimpleInputPlugin] )
//    {
//        WTSimpleYUVInputCamera *simpleInputCamera = objc_getAssociatedObject(self, kWTAugmentedRealityViewController_AssociatedSimpleYUVInputCameraWrapperKey);
//        if ( simpleInputCamera )
//        {
//            [self.architectView removePlugin:[simpleInputCamera cameraInputPlugin]];
//        }
//    }
//    else if ([pluginName isEqualToString:kWTPluginIdentifier_CustomCameraPlugin] )
//    {
//        WTYUVInputCamera *yuvInputCamera = objc_getAssociatedObject(self, kWTAugmentedRealityViewController_AssociatedYUVInputCameraWrapperKey);
//        if (yuvInputCamera)
//        {
//            [self.architectView removePlugin:[yuvInputCamera cameraInputPlugin]];
//        }
    }
    else if ( [pluginName isEqualToString:kWTPluginIdentifier_MarkerTrackingPlugin] )
    {
        [self.architectView removeNamedPlugin:kWTPluginIdentifier_MarkerTrackingPlugin];
    }
}

@end
