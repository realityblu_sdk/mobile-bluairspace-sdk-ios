//
//  WTAugmentedRealityViewController+ScreenshotSharing.m
//  SDKExamples
//
//  Created by Andreas Schacherbauer on 25/09/14.
//  Copyright (c) 2014 Wikitude. All rights reserved.
//

#import "WTAugmentedRealityViewController+ScreenshotSharing.h"

#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import <BLUs/BLUs-Swift.h>

@implementation WTAugmentedRealityViewController (ScreenshotSharing)


#pragma mark - Public Methods

- (void)captureScreen {
    NSDictionary* info = @{};
    [self.architectView captureScreenWithMode: WTScreenshotCaptureMode_CamAndWebView usingSaveMode:WTScreenshotSaveMode_PhotoLibrary saveOptions:WTScreenshotSaveOption_CallDelegateOnSuccess context:info];
}

#pragma mark - Delegation
#pragma mark WTArchitectView

- (void)architectView:(WTArchitectView *)architectView didCaptureScreenWithContext:(NSDictionary *)context
{
    UIImage* image = (UIImage *)[context objectForKey:kWTScreenshotImageKey];
    WTScreenshotSaveMode saveMode = [[context objectForKey:kWTScreenshotSaveModeKey] unsignedIntegerValue];
    
    switch (saveMode)
    {
        case WTScreenshotSaveMode_Delegate:
            [self postImageOnFacebook:image];
            break;
            
        case WTScreenshotSaveMode_PhotoLibrary:
            [self showPhotoLibraryAlert];
            break;
            
        default:
            break;
    }
    
}

- (void)architectView:(WTArchitectView *)architectView didFailCaptureScreenWithError:(NSError *)error
{
    NSLog(@"Error capturing screen: %@", error);
}

#pragma mark - Private Methods

- (void)postImageOnFacebook:(UIImage *)image
{
    SLComposeViewController* composerSheet = [SLComposeViewController composeViewControllerForServiceType: SLServiceTypeFacebook];
    
    [composerSheet setInitialText:@"Wikitude screenshot"];
    [composerSheet addImage:image];
    [composerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        NSString *output;
        switch (result)
        {
            case SLComposeViewControllerResultDone:
                output = @"Post Successfull";
            break;
            
            case SLComposeViewControllerResultCancelled:
                output = @"Action Cancelled";
            break;
                
            default:
            break;
        }

        UIAlertController *facebookStatusNotificationController = [UIAlertController alertControllerWithTitle:@"Facebook" message:output preferredStyle:UIAlertControllerStyleAlert];
        [facebookStatusNotificationController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];

        [self presentViewController:facebookStatusNotificationController animated:YES completion:nil];
    }];
    
    [self presentViewController:composerSheet animated:YES completion:nil];
}

- (void)showPhotoLibraryAlert
{
    UIAlertController *photoLibraryStatusNotificationController = [UIAlertController alertControllerWithTitle:@"Success" message:@"Screenshot was stored in your photo library" preferredStyle:UIAlertControllerStyleAlert];
    [photoLibraryStatusNotificationController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
//    [self presentViewController:photoLibraryStatusNotificationController forArchitectView:self.architectView];
}

@end
