#import "WTAugmentedRealityViewController.h"


@interface WTAugmentedRealityViewController (ApplicationModelExample) <CLLocationManagerDelegate>

- (void)startLocationUpdatesForPoiInjection;

@end
