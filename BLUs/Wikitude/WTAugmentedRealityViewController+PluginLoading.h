#import "WTAugmentedRealityViewController.h"

extern NSString * const kWTPluginIdentifier_BarcodePlugin;
extern NSString * const kWTPluginIdentifier_FacedetectionPlugin;
extern NSString * const kWTPluginIdentifier_SimpleInputPlugin;
extern NSString * const kWTPluginIdentifier_CustomCameraPlugin;
extern NSString * const kWTPluginIdentifier_MarkerTrackingPlugin;

@class WTAugmentedRealityExperience;

@interface WTAugmentedRealityViewController (PluginLoading)

- (void)loadNamedPlugin:(NSString *)pluginName forAugmentedRealityExperience:(WTAugmentedRealityExperience *)augmentedRealityExperience;

- (void)startNamedPlugin:(NSString *)pluginName;
- (void)stopNamedPlugin:(NSString *)pluginName;

@end
