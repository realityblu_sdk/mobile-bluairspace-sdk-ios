#import <Foundation/Foundation.h>

@interface NSString (isNumeric)

- (BOOL) isAllDigits;
- (BOOL) isNumeric;

@end
