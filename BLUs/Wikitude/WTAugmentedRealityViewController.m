#import "WTAugmentedRealityViewController.h"
#import "WTAugmentedRealityExperience.h"
#import "WTAugmentedRealityViewController+ExampleCategoryManagement.h"

#import "BLUs.h"
#import "NSString+isNumeric.h"
#import "UIViewController+Extension.h"
#import <BLUs/BLUs-Swift.h>

#import <BLUs/WTAugmentedRealityExperiencesManager.h>
#import <BLUs/WTAugmentedRealityViewController.h>
#import <BLUs/WTAugmentedRealityExperience.h>

#import <BLUs/WTAugmentedRealityExperiencesManager+ExampleSections.h>
#import <BLUs/WTAugmentedRealityExperiencesManager+HostSections.h>
#import <BLUs/WTSegueDefinitions.h>
#import <BLUs/WTCoreServices.h>
#import <BLUs/WTAugmentedRealityViewController+ApplicationModelExample.h>
#import <BLUs/WTAugmentedRealityViewController+ExampleCategoryManagement.h>
#import <BLUs/WTAugmentedRealityViewController+PluginLoading.h>
#import <BLUs/WTAugmentedRealityViewController+ScreenshotSharing.h>


@class L;
@class SwiftFromObjCBridge;
@class DownloadUtil;
@class BtnCalendarAction;
@class PlistManager;


@interface WTAugmentedRealityViewController () {
    BOOL isWorldInitialized;
    BOOL isMakingPhoneCall;
    BOOL isAppInBackground;
}

- (void)onDownloadScanDataStarted;
- (void)onDownloadScanDataFinished;
- (void)flashlightNative;
- (void)endTrial;
- (void)showScanningDialog;
- (void)initializeWorld;

- (void)modelPlayStart: (int) modelId;
- (void)modelPlayStopAll;
- (void)experiencePlayStart: (int) expId;
- (void)experiencePlayStop;

- (void)sendAnalyticsAction:(int)expId
                           :(int)type
                           :(id)contentId
                           :(id)name
                           :(id)value;
- (void)sendPauseEvent;
- (void)sendResumeEvent;


@property (nonatomic, assign) BOOL                                          showsBackButtonOnlyInNavigationItem;
@property (nonatomic, weak) WTAugmentedRealityExperience                    *augmentedRealityExperience;
@property (nonatomic, copy) WTNavigation                                    *loadedArchitectWorldNavigation;
@property (nonatomic, weak) WTNavigation                                    *loadingArchitectWorldNavigation;
@property (nonatomic, assign) AVCaptureDevicePosition                       currentlyActiveCaptureDevicePosition;
//@property (nonatomic, assign) BOOL shouldNavigateBackOnResume; //0 - for scan, 1 - for instant tracking
@end


@implementation WTAugmentedRealityViewController

- (void)dealloc {
    [L debugWithMessage:NSStringFromSelector(_cmd)];
    [self stopArchitectViewRendering];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)deinitializeArchitectView {
    self.architectView.delegate = nil;
    [self.architectView removeFromSuperview];
    self.architectView = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [L debugWithMessage:NSStringFromSelector(_cmd)];

    // Do any additional setup after loading the view.
    
    /* Architect view specific initialization code
     * - a license key is set to enable all SDK features
     * - the architect view delegate is set so that the application can react on SDK specific events
     */

    [self.architectView setLicenseKey: [SwiftFromObjCBridge sInst].licenseKey];
    self.architectView.delegate = self;
    
    self.currentlyActiveCaptureDevicePosition = AVCaptureDevicePositionUnspecified;

    /* The architect view needs to be paused/resumed when the application is not active. We use UIApplication specific notifications to pause/resume architet view rendering depending on the application state */
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveApplicationWillResignActiveNotification:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveApplicationDidBecomeActiveNotification:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveApplicationDidEnterBackgroundNotification:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveApplicationSuspendedNotification:) name:@"UIApplicationSuspendedNotification" object:nil];
    
    isArSet = false;
    
    isWorldInitialized = NO;
    isAppInBackground = NO;
    isMakingPhoneCall = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [L debugWithMessage:NSStringFromSelector(_cmd)];

    self.augmentedRealityExperience = [SwiftFromObjCBridge sInst].arExperience;
    // self.shouldNavigateBackOnResume = [SwiftFromObjCBridge sInst].shouldNavigateBackOnResume;
    self.showsBackButtonOnlyInNavigationItem = YES;
    if (!isWorldInitialized) {
        [self loadExampleSpecificCategoryForAugmentedRealityExperience:self.augmentedRealityExperience];
    }

    NSString *navigationItemTitle = self.augmentedRealityExperience.title;
    if ( UIUserInterfaceIdiomPad == [[UIDevice currentDevice] userInterfaceIdiom] )
    {
        NSString *urlString = [self.augmentedRealityExperience.URL isFileURL] ? @"" : [NSString stringWithFormat:@" - %@", [self.augmentedRealityExperience.URL absoluteString]];
        navigationItemTitle = [NSString stringWithFormat:@"%@%@", self.augmentedRealityExperience.title, urlString];
    }
    self.navigationItem.title = @"";
    
    if ( self.showsBackButtonOnlyInNavigationItem )
    {
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    /* Setting the interativePopGestureRecognizer delegate and implementing the -gestureRecognizer:shouldRecognizeSimultaneouslyWithGestureRecognizer like shown in the vc is important for Wikitude gestures to work */
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
   
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    [self.architectView setShouldRotate:YES toInterfaceOrientation:orientation];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    [L debugWithMessage:NSStringFromSelector(_cmd)];
    
    if (!isWorldInitialized) {
        /* Depending on the currently load architect world a new world will be loaded */
        [self checkExperienceLoadingStatus];
        /* And the Wikitude SDK rendering is resumed if it’s currently paused */
        [self startArchitectViewRendering];
        /* Just to make sure that the Wikitude SDK is rendering in the correct interface orientation just in case the orientation changed while this view was not visible */
        [self initializeWorld];
        
        isWorldInitialized = YES;
    }
}

- (void)dismissController {
    [self onReceivedActionDismiss];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [L debugWithMessage:NSStringFromSelector(_cmd)];
    /* iOS 8 requires to set the interactivePopGestureRecognizer to nil in order to prevent a crash in the underlying vc */
    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [L debugWithMessage:NSStringFromSelector(_cmd)];
}


#pragma mark - Layout / Rotation

/* Wikitude SDK Rotation handling
 *
 * viewWillTransitionToSize -> iOS 8 and newer
 * willRotateToInterfaceOrientation -> iOS 7
 *
 * Overriding both methods seems to work for all the currently supported iOS Versions
 * (7, 8, 9). The run-time version check is included nonetheless. Better safe than sorry.
 */

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    if (coordinator)
    {
        [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context)
         {
             UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
             [self.architectView setShouldRotate:YES toInterfaceOrientation:orientation];
             
         } completion:^(id<UIViewControllerTransitionCoordinatorContext> context)
         {
             
         }];
    } else {
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        [self.architectView setShouldRotate:YES toInterfaceOrientation:orientation];
    }
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    //  if iOS 8.0 or greater according to http://stackoverflow.com/questions/3339722/how-to-check-ios-version
    if ([[[UIDevice currentDevice] systemVersion] compare:@"8.0" options:NSNumericSearch] != NSOrderedAscending) {
        return;
    }
    [self.architectView setShouldRotate:YES toInterfaceOrientation:toInterfaceOrientation];
}

#pragma mark - Delegation
#pragma mark WTArchitectView

/* This architect view delegate method is used to keep the currently loaded architect world url. Every time this view becomes visible again, the controller checks if the current url is not equal to the new one and then loads the architect world */
- (void)architectView:(WTArchitectView *)architectView didFinishLoadArchitectWorldNavigation:(WTNavigation *)navigation {
    if ( [self.loadingArchitectWorldNavigation isEqual:navigation] )
    {
        self.loadedArchitectWorldNavigation = navigation;
    }
}

- (void)architectView:(WTArchitectView *)architectView didFailToLoadArchitectWorldNavigation:(WTNavigation *)navigation withError:(NSError *)error {
    NSLog(@"Architect view '%@' \ndid fail to load navigation '%@' \nwith error '%@'", architectView, navigation, error);
}

/* As mentioned before, some architect examples require iOS specific implementation details.
 * Here is the decision made which iOS specific details should be executed
 */

- (void)architectViewCallJavaScript:(NSString *)cmd {
    [self.architectView callJavaScript:cmd];
}

- (void)architectViewCallJavaScriptAsync:(NSString *)cmd {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.architectView callJavaScript:cmd];
    });
}


- (void)architectView:(WTArchitectView *)architectView receivedJSONObject:(NSDictionary *)jsonObject
{
    if ( jsonObject ) {
        if ( [[jsonObject objectForKey:@"name"] isEqualToString:@"LogMessageFromWikitudeJS"]  ) {
            NSString *messageDescription = [jsonObject objectForKey:@"description"];
        } else  if ( [[jsonObject objectForKey:@"action"] isEqualToString:@"back_to_gallery"]  ) {
            [self dismissController];
        }  else  if ( [[jsonObject objectForKey:@"action"] isEqualToString:@"flip_camera"]  ) {
            
        }  else  if ( [[jsonObject objectForKey:@"action"] isEqualToString:@"experienceAction"]  ) {
            
            NSDictionary *event = [jsonObject objectForKey:@"param"];
            
            //id actionObject = [event objectForKey:@"action"];
            
            int expId = [[event objectForKey:@"expId"] integerValue];
            int type = [[event objectForKey:@"type"] integerValue];
            id contentId = [event objectForKey:@"id"];
            id name = [event objectForKey:@"name"];
            id value = [event objectForKey:@"value"];
            
            [self sendAnalyticsAction:expId :type :contentId :name :value];
        }  else  if ( [[jsonObject objectForKey:@"action"] isEqualToString:@"btn_action"]  ) { //markerbased Button action
            int actionMode = [[jsonObject objectForKey:@"mode"] integerValue];
            if (actionMode == 0) {
                @try {
                    NSString *urlString = [jsonObject objectForKey:@"param"];
                    [self onReceivedButtonActionOpenExternalUrl:urlString];
                } @catch (NSException *exception) {
                    NSLog(@"Cannot perform action <open-external-url> for the reason: %@", exception.reason);
                }
            } else if (actionMode == 4) { //phone
                NSString *phone = [jsonObject objectForKey:@"param"];
                isMakingPhoneCall = YES;
                [self onReceivedButtonActionPhone: phone];
            } else if (actionMode == 5) { //email
                @try {
                    NSString *emailTo;
                    NSString *emailSubject;
                    if ([[jsonObject objectForKey:@"param"] isKindOfClass:[NSString class]]) {
                        emailTo = [jsonObject objectForKey:@"param"];
                        emailSubject = @"";
                    } else {
                        NSDictionary *emailData = [jsonObject objectForKey:@"param"];
                        emailTo = [emailData objectForKey:@"emailTo"];
                        emailSubject = [emailData objectForKey:@"emailSubject"];
                    }
                    [self onReceivedButtonActionEmail: emailTo subject: emailSubject];
                } @catch (NSException *exception) {
                    NSLog(@"Cannot perform action <send-email> for the reason: %@", exception.reason);
                }
            } else if (actionMode == 6) { //calendar
                if (![PlistManager hasPropertyWithProperty:@"NSCalendarsUsageDescription"]) {
                    NSLog(@"[BLUairspace] The app's Info.plist must contain an NSCalendarsUsageDescription key with a string value explaining to the user how the app uses this data.");
                    return;
                }
                NSDictionary *calendar = [jsonObject objectForKey:@"param"];
                
                NSString *title = [calendar objectForKey:@"title"];
                NSString *desc = [calendar objectForKey:@"description"];
                NSString *start = [calendar objectForKey:@"startDate"];
                NSString *end = [calendar objectForKey:@"endDate"];
                [self onReceivedButtonActionCalendarWithTitle:title :start :end :desc];
                
            } else if (actionMode == 8) { //vcard
                NSDictionary *vcard = [jsonObject objectForKey:@"param"];
                NSString *name = [vcard objectForKey:@"name"];
                NSString *company = [vcard objectForKey:@"company"];
                NSString *title = [vcard objectForKey:@"title"];
                NSString *phoneType = [vcard objectForKey:@"phone_type"];
                NSString *phone = [vcard objectForKey:@"phone"];
                NSString *emailType = [vcard objectForKey:@"email_type"];
                NSString *email = [vcard objectForKey:@"email"];
                [self onReceivedButtonActionVCardWithName:name :company :title :phoneType :phone :emailType :email];
            }
        }  else  if ( [[jsonObject objectForKey:@"action"] isEqualToString:@"SceneDownload"]  ) {
            [self startDownloadingMarkerbasedData: jsonObject];
        } else  if ( [[jsonObject objectForKey:@"action"] isEqualToString:@"PersonalVideo"]  ) {
            [self getUserPersonalVideo: jsonObject];
            
        } else  if ( [[jsonObject objectForKey:@"action"] isEqualToString:@"changeTextStatus"]  ) {
            [[SwiftFromObjCBridge sInst] activateMarkerLessExperienceDoNotShowTips];
            
        } else  if ( [[jsonObject objectForKey:@"action"] isEqualToString:@"showTutorial"]  ) {
            BOOL isNeed = [[jsonObject objectForKey:@"param"] boolValue];
            [[SwiftFromObjCBridge sInst] setNeedMarkerBasedTutorialWithValue:(isNeed)];
            
        } else if ( [[jsonObject objectForKey:@"action"] isEqualToString:@"ClientData"] ) {
            
            //send client token and collection id to JS
            NSMutableString *methodCall = [NSMutableString stringWithString:@"NativeBridge.setupClientData(\""];
            NSString *clientToken = [[SwiftFromObjCBridge sInst] getMarkerbasedClientToken];
            NSString *collectionId = [[SwiftFromObjCBridge sInst] getMarkerbasedCollectionId];
            NSString *groupId = [[SwiftFromObjCBridge sInst] getMarkerbasedGroupId];
            NSString *proofingCollectionId = [[SwiftFromObjCBridge sInst] getProofingCollectionId];
            NSString *markerlessMarkerBasedUrl = [[SwiftFromObjCBridge sInst] getMarkerlessMarkerbasedUrl];
            if ([groupId length] == 0) {
                NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
            
                NSArray *items = @[@"com.ar.realityblu", @"com.prod.SolimarAR", @"com.ar.gide"];
                int item = [items indexOfObject:bundleIdentifier];
                switch (item) {
                    case 0:
                         groupId = @"rkjUh00A-";
                        break;
                    case 1:
                         groupId = @"HyCnhRCRW";
                        break;
                    case 2:
                         groupId = @"HJxzasWaz";
                        break;
                    default:
                        groupId = @"Other";
                        break;
                }
            }
            NSString *collId = nil;
            if (_isProofing) {
                collId = proofingCollectionId;
            } else {
                collId = collectionId;
            }
            [methodCall appendString:clientToken];
            [methodCall appendString:@","];
            [methodCall appendString:collId];
            [methodCall appendString:@","];
            [methodCall appendString:groupId];
            [methodCall appendString:@","];
            [methodCall appendString:markerlessMarkerBasedUrl];
            [methodCall appendString:@"\")"];
            
            [self architectViewCallJavaScript:methodCall];
        }  else if ( [[jsonObject objectForKey:@"action"] isEqualToString:@"experiencePlayStart"] ) {
            
            //NSInteger *expId = [jsonObject objectForKey:@"param"];
            int expId = [[jsonObject objectForKey:@"param"] integerValue];
            [self experiencePlayStart: expId];
            
        }  else if ( [[jsonObject objectForKey:@"action"] isEqualToString:@"experiencePlayStop"] ) {
            [self experiencePlayStop];
            
        }  else if ( [[jsonObject objectForKey:@"action"] isEqualToString:@"modelPlayStart"] ) {
            int modelId = [[jsonObject objectForKey:@"param"] integerValue];
            [self modelPlayStart: modelId];
            
        }  else if ( [[jsonObject objectForKey:@"action"] isEqualToString:@"modelPlayStopAll"] ) {
            [self modelPlayStopAll];
            
        }  else if ( [[jsonObject objectForKey:@"action"] isEqualToString:@"end_trial"] ) {
           
            [self endTrial];
        }  else if ( [[jsonObject objectForKey:@"action"] isEqualToString:@"flashlightNative"] ) {
            
            [self flashlightNative];
        }  else if ( [[jsonObject objectForKey:@"action"] isEqualToString:@"scan_qr_code"] ) {
                  
            [self showScanningDialog];
        }
    }
    
    //this functionality was here before
    id actionObject = [jsonObject objectForKey:@"action"];
    if ( actionObject
        &&
        [actionObject isKindOfClass:[NSString class]]
        )
    {
        NSString *action = (NSString *)actionObject;
        if ( [action isEqualToString:@"capture_screen"] ) {
            [self printScreenAction];
        }
    }
}

- (void)getUserPersonalVideo:(NSDictionary *)jsonObject
{
    [SwiftFromObjCBridge sInst].onPersonalVideoDownloaded = ^{
        [self sendPersonalVideoUrlToJS];
    };
    
    [[MarkerbasedDownloadUtililty sharedInstance] doDownloadPersonalVideoWithDataJson:jsonObject];
}

- (void)sendPersonalVideoUrlToJS
{
    NSString *urlString = [[SwiftFromObjCBridge sInst] personalVideoDownloaded];
    NSString *videoId = [[SwiftFromObjCBridge sInst] personalVideoDownloadedId];
    
    BOOL isVideoIdNumber = [videoId isNumeric];

    NSMutableString *methodCall;
    
    if (isVideoIdNumber){
        NSString *videoIdString = [NSString stringWithFormat: @"%d", videoId];
    
        methodCall = [NSMutableString stringWithString:@"NativeBridge.onPersonalVideoLoaded("];
        [methodCall appendString:videoId];
        [methodCall appendString:@",\'"];
        [methodCall appendString:urlString];
        [methodCall appendString:@"\')"];
        
    } else {
        methodCall = [NSMutableString stringWithString:@"NativeBridge.onPersonalVideoLoaded(\'"];
        [methodCall appendString:videoId];
        [methodCall appendString:@"\',\'"];
        [methodCall appendString:urlString];
        [methodCall appendString:@"\')"];
    }
    [self architectViewCallJavaScript:methodCall];
}

- (void)startDownloadingMarkerbasedData:(NSDictionary *)jsonObject
{
    __weak WTAugmentedRealityViewController *weakSelf = self;
    if ([self respondsToSelector:@selector(onDownloadScanDataStarted)]) {
        [self onDownloadScanDataStarted];
    }
    
    [SwiftFromObjCBridge sInst].onCachedUrlsReady = ^{
        [weakSelf sendCachedFilesPathsToJS];
    };
    
    [SwiftFromObjCBridge sInst].onNextFileLoaded = ^{
        [weakSelf sendNextLoadedFilePathToJS];
    };
    
    [SwiftFromObjCBridge sInst].onLoadingComplete = ^{
        [weakSelf sendLoadingCompleteToJS];
    };

    [[MarkerbasedDownloadUtililty sharedInstance] doDownloadDataWithDataJson:jsonObject onCancelDownload:^{
        if ([weakSelf respondsToSelector:@selector(onDownloadScanDataFinished)]) {
            [weakSelf onDownloadScanDataFinished];
        }
    }];
}

- (void)sendCachedFilesPathsToJS
{
    NSString *jsonString = [[SwiftFromObjCBridge sInst] scanExperiencesCached];
    
    NSMutableString *methodCall = [NSMutableString stringWithString:@"NativeBridge.onCachedUrlsReady(\'"];
    [methodCall appendString:jsonString];
    [methodCall appendString:@"\')"];

    [self architectViewCallJavaScript:methodCall];
}

- (void)sendNextLoadedFilePathToJS
{
    NSString *jsonString = [[SwiftFromObjCBridge sInst] scanExperienceDownloaded];
    
    NSMutableString *methodCall = [NSMutableString stringWithString:@"NativeBridge.onNextFileLoaded(\'"];
    [methodCall appendString:jsonString];
    [methodCall appendString:@"\')"];

    [self.architectView callJavaScript:(methodCall)];
}

- (void)sendLoadingCompleteToJS
{
    NSMutableString *methodCall = [NSMutableString stringWithString:@"NativeBridge.onFilesLoadingComplete()"];
    [self architectViewCallJavaScript:methodCall];
}

/* Use this method to implement/show your own custom device sensor calibration mechanism.
 *  You can also use the system calibration screen, but pls. read the WTStartupConfiguration documentation for more details.
 */
- (void)architectViewNeedsDeviceSensorCalibration:(WTArchitectView *)architectView
{
    NSLog(@"Device sensor calibration needed. Rotate the device 360 degree around it's Y-Axis");
}

/* When this method is called, the device sensors are calibrated enough to deliver accurate values again.
 * In case a custom calibration screen was shown, it can now be dismissed.
 */
- (void)architectViewFinishedDeviceSensorsCalibration:(WTArchitectView *)architectView
{
    NSLog(@"Device sensors calibrated");
}

- (void)architectView:(WTArchitectView *)architectView didSwitchToActiveCaptureDevicePosition:(AVCaptureDevicePosition)activeCaptureDevicePosition {
    self.currentlyActiveCaptureDevicePosition = activeCaptureDevicePosition;
}

#pragma mark UIGestureRecognizer

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - Notifications
/* UIApplication specific notifications are used to pause/resume the architect view rendering */
- (void)didReceiveApplicationWillResignActiveNotification:(NSNotification *)notification {
    [L debugWithMessage:@"willResignActive"];
    
    if (isMakingPhoneCall) {
        [self stopArchitectViewRendering];
    }
    
    [self sendPauseEvent];
}

- (void)didReceiveApplicationDidBecomeActiveNotification:(NSNotification *)notification {
    [L debugWithMessage:@"becomeActive"];
    
    if (isAppInBackground) {
        [self startArchitectViewRendering];
        isAppInBackground = NO;
    }
    
    if (isMakingPhoneCall) {
        [self startArchitectViewRendering];
        isMakingPhoneCall = NO;
    }
    
    [self sendResumeEvent];
}

- (void)didReceiveApplicationDidEnterBackgroundNotification:(NSNotification *)notification {
    [L debugWithMessage:@"enterBackground"];
}

- (void)didReceiveApplicationSuspendedNotification:(NSNotification *)notification {
    [L debugWithMessage:@"suspended"];
    
    isAppInBackground = YES;
    [self stopArchitectViewRendering];
}


#pragma mark - Private Methods

/* The method that is invoked everytime the view becomes visible again and then decides if a new architect world needs to be loaded or not */
- (void)checkExperienceLoadingStatus {
    if ( ![self.loadedArchitectWorldNavigation.originalURL isEqual:self.augmentedRealityExperience.URL] )
    {
        self.architectView.requiredFeatures = self.augmentedRealityExperience.requiredFeatures;
        self.loadingArchitectWorldNavigation = [self.architectView loadArchitectWorldFromURL:self.augmentedRealityExperience.URL];
    }
}


BOOL isArSet = false;

/* The next two methods actually start/stop architect view rendering */

- (void)startArchitectViewRendering {
    if ( ![self.architectView isRunning] ) {
        [L infoWithMessage:@"startArchitectViewRendering"];
        [self startExampleSpecificCategoryForAugmentedRealityExperience:self.augmentedRealityExperience];

        [self.architectView start:^(WTArchitectStartupConfiguration *configuration)
        {
            if ( self.currentlyActiveCaptureDevicePosition != AVCaptureDevicePositionUnspecified )
            {
                configuration.captureDevicePosition = self.currentlyActiveCaptureDevicePosition;
            }
            else
            {
                NSString *captureDevicePositionString = [self.augmentedRealityExperience.startupParameters objectForKey:kWTAugmentedRealityExperienceStartupParameterKey_CaptureDevicePosition];
                configuration.captureDevicePosition = [captureDevicePositionString isEqualToString:kWTAugmentedRealityExperienceStartupParameterKey_CaptureDevicePosition_Back] ? AVCaptureDevicePositionBack : AVCaptureDevicePositionFront;
            }
            
            NSString *captureDeviceResolutionString = [self.augmentedRealityExperience.startupParameters objectForKey:kWTAugmentedRealityExperienceStartupParameterKey_CaptureDeviceResolution];
            if ( captureDeviceResolutionString )
            {
                if ( [kWTAugmentedRealityExperienceStartupParameterKey_CaptureDeviceResolution_Auto isEqualToString:captureDeviceResolutionString] )
                {
                    configuration.captureDeviceResolution = WTCaptureDeviceResolution_AUTO;
                }
                else if ( [kWTAugmentedRealityExperienceStartupParameterKey_CaptureDeviceResolution_SD_640x480 isEqualToString:captureDeviceResolutionString] )
                {
                    configuration.captureDeviceResolution = WTCaptureDeviceResolution_SD_640x480;
                }
                else if ( [kWTAugmentedRealityExperienceStartupParameterKey_CaptureDeviceResolution_HD_1280x720 isEqualToString:captureDeviceResolutionString] )
                {
                    configuration.captureDeviceResolution = WTCaptureDeviceResolution_HD_1280x720;
                }
                else if ( [kWTAugmentedRealityExperienceStartupParameterKey_CaptureDeviceResolution_FULL_HD_1920x1080 isEqualToString:captureDeviceResolutionString] )
                {
                    configuration.captureDeviceResolution = WTCaptureDeviceResolution_FULL_HD_1920x1080;
                }
            }
            
            NSString *captureDeviceManualFocusDistanceString = [self.augmentedRealityExperience.startupParameters objectForKey:kWTAugmentedRealityExperienceStartupParameterKey_ManualFocusDistance];
            configuration.captureDeviceFocusDistance = [captureDeviceManualFocusDistanceString floatValue];
        }
                      completion:^(BOOL isRunning, NSError *error) { }];
    }
    
    //send markerless experience models paths to JS
    BOOL didDoNotShowTipsActivated = [[SwiftFromObjCBridge sInst] getMarkerLessExperienceDoNotShowTipsActivated];
    
    NSArray *location = [[SwiftFromObjCBridge sInst] getKnownLocation];
    NSMutableString *locationCall = [NSMutableString stringWithString:@"NativeBridge.setLastLocation("];
    [locationCall appendString: [location[0] stringValue]];
    [locationCall appendString:@","];
    [locationCall appendString: [location[1] stringValue]];
    [locationCall appendString:@")"];
    //    NSLog(@"qqq - LOCATION JS Method to call: %@", locationCall);
    [self architectViewCallJavaScript:locationCall];
    
    NSString *pathsArrayString = [[SwiftFromObjCBridge sInst] experiencesPathsArrayString];
    if (pathsArrayString != nil && pathsArrayString.length > 0) {
        NSMutableString *pathsCall = [NSMutableString stringWithString:@"World.takeData(\'"];
        [pathsCall appendString:pathsArrayString];
        [pathsCall appendString:@"\',"];
        
        if (!didDoNotShowTipsActivated) {
            [pathsCall appendString:@"\'true\'"];
        } else {
            [pathsCall appendString:@"\'false\'"];
        }
        
        [pathsCall appendString:@")"];
        
        //  NSLog(@"qqq - Instant tracking OUTPUT: JS Method to call: %@", pathsCall);
        
        [self architectViewCallJavaScript:pathsCall];
    } else {
        //   NSLog(@"start AR rendering, nothing to send");
    }
    
    
    int arType = [[SwiftFromObjCBridge sInst] arType];
    if(arType == 1 && !isArSet){
        
        BOOL isTutorial = [[SwiftFromObjCBridge sInst] getIsNeedMarkerBasedTutorial];
        NSString *isTutorialString = isTutorial ? @"true" : @"false";
        if (isTutorial) {
             NSMutableString *methodCall = [NSMutableString stringWithString:@"checkInitialLogic(true)"];
            [self architectViewCallJavaScript:methodCall];
        } else {
             NSMutableString *methodCall = [NSMutableString stringWithString:@"checkInitialLogic(false)"];
             [self architectViewCallJavaScript:methodCall];
        }
        
        NSString *emailUserSelf = [[SwiftFromObjCBridge sInst] getEmailUserSelfFromPref];
        if ([emailUserSelf length] > 0) {
            NSMutableString *methodCall = [NSMutableString stringWithString:@"NativeBridge.setTrial()"];
            [self architectViewCallJavaScript:methodCall];
        }
        
        isArSet = true;
    }
}

- (void)stopArchitectViewRendering {
    if ( [self.architectView isRunning] ) {
        [L infoWithMessage:@"stopArchitectViewRendering"];
        [self.architectView stop];
        [self stopExampleSpecificCategoryForAugmentedRealityExperience:self.augmentedRealityExperience];
    }
}

- (void)printScreenAction {
    if ([PlistManager hasPropertyWithProperty:@"NSPhotoLibraryAddUsageDescription"]) {
        NSDictionary* info = @{};
        [self captureScreen];
//        [self.architectView captureScreenWithMode: WTScreenshotCaptureMode_CamAndWebView usingSaveMode:WTScreenshotSaveMode_PhotoLibrary saveOptions:WTScreenshotSaveOption_CallDelegateOnSuccess context:info];
    } else {
        NSLog(@"[BLUairspace] The app's Info.plist must contain an NSPhotoLibraryAddUsageDescription key with a string value explaining to the user how the app uses this data.");
    }
}

@end
