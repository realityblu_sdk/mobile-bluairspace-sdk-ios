//
//  WTAugmentedRealityExperiencesManager+ExampleSections.m
//  SDKExamples
//
//  Created by Andreas Schacherbauer on 23/09/14.
//  Copyright (c) 2014 Wikitude. All rights reserved.
//

#import "WTAugmentedRealityExperiencesManager+ExampleSections.h"
#import "WTAugmentedRealityExperience.h"


@implementation WTAugmentedRealityExperiencesManager (ExampleSections)


#pragma mark - Public Methods

- (BOOL)hasLoadedAugmentedRealityExperiencesFromPlist
{
    return [self numberOfSections] > 0;
}

- (void)loadARExperience:(WTAugmentedRealityExperiencesLoadCompletionHandler)completionHandler scanCameraPositionBack:(BOOL)campos;
{
    [self loadMarkerlessExperience];
    [self loadScanExperience:campos];
//    [self loadGeoExperience];
    
    if ( completionHandler )
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            completionHandler(0);
        });
    }
}

- (void)loadMarkerlessExperience
{
    NSString *title = @"Interactivity Instant";
    NSString *groupTitle = @"Instant Tracking";
    NSString *relativePath = @"markerless";
    
    WTFeatures features = 0;
    features |= WTFeature_InstantTracking;
    WTFeatures requiredFeatures = features;
    
    NSDictionary *dict = @{@"camera_position" : @"back"};
    NSDictionary *startupParameters = dict;
    
    NSString *bundleSubdirectory = [[NSString stringWithFormat:@"wikitude"] stringByAppendingPathComponent:relativePath];

    NSURL *absoluteURL = [[NSBundle bundleForClass:[self class]] URLForResource:@"index" withExtension:@"html" subdirectory:bundleSubdirectory];
    
    WTAugmentedRealityExperience *experience = [WTAugmentedRealityExperience experienceWithTitle:title
                                                                                      groupTitle:groupTitle
                                                                                             URL:absoluteURL
                                                                                requiredFeatures:requiredFeatures
                                                                               startupParameters:startupParameters
                                                                 requiredViewControllerExtension:nil];
    
    [self addAugmentedRealityExperience:experience inSection:0 moveToTop:NO];
}

- (void)loadScanExperience:(BOOL)scanCameraPositionBack
{
    NSString *title = @"Interactivity";
    NSString *groupTitle = @"3D Models";
    NSString *relativePath = @"markerbased";
    
    WTFeatures features = 0;
    features |= WTFeature_ImageTracking;
    
    WTFeatures requiredFeatures = features;
    
    NSMutableString *cameraPosition =  [NSMutableString stringWithString:@"back"];
    if (!scanCameraPositionBack) {
        cameraPosition =  [NSMutableString stringWithString:@"front"];
    }
    
    NSDictionary *dict = @{@"camera_position" : [NSString stringWithString:cameraPosition], @"camera_resolution" : @"auto"};
    NSDictionary *startupParameters = dict;
    
    NSString *bundleSubdirectory = [[NSString stringWithFormat:@"wikitude"] stringByAppendingPathComponent:relativePath];
    NSURL *absoluteURL = [[NSBundle bundleForClass:[self class]] URLForResource:@"index" withExtension:@"html" subdirectory:bundleSubdirectory];
    
    WTAugmentedRealityExperience *experience = [WTAugmentedRealityExperience experienceWithTitle:title
                                                                                      groupTitle:groupTitle
                                                                                             URL:absoluteURL
                                                                                requiredFeatures:requiredFeatures
                                                                               startupParameters:startupParameters
                                                                 requiredViewControllerExtension:nil];
    
    [self addAugmentedRealityExperience:experience inSection:0 moveToTop:NO];
}


//- (void)loadGeoExperience
//{
//    NSString *title = @"GeoTitle";
//    NSString *groupTitle = @"GeoGroup";
//    NSString *relativePath = @"Geo";
//
//    WTFeatures features = 0;
//    features |= WTFeature_Geo;
//
//    WTFeatures requiredFeatures = features;
//
//    NSMutableString *cameraPosition =  [NSMutableString stringWithString:@"back"];
//
//    NSDictionary *dict = @{@"camera_position" : [NSString stringWithString:cameraPosition], @"camera_resolution" : @"auto"};
//    NSDictionary *startupParameters = dict;
//
//    NSString *bundleSubdirectory = [[NSString stringWithFormat:@"Assets/wikitude"] stringByAppendingPathComponent:relativePath];
//    NSURL *absoluteURL = [[NSBundle mainBundle] URLForResource:@"index" withExtension:@"html" subdirectory:bundleSubdirectory];
//
//    WTAugmentedRealityExperience *experience = [WTAugmentedRealityExperience experienceWithTitle:title
//                                                                                      groupTitle:groupTitle
//                                                                                             URL:absoluteURL
//                                                                                requiredFeatures:requiredFeatures
//                                                                               startupParameters:startupParameters
//                                                                 requiredViewControllerExtension:nil];
//
//    [self addAugmentedRealityExperience:experience inSection:0 moveToTop:NO];
//}

@end
