#import "WTAugmentedRealityExperiencesManager.h"


@interface WTAugmentedRealityExperiencesManager (HostSections)

- (instancetype)initWithHostSetup;
- (void)removeHostForSection:(NSInteger)section;

- (NSInteger)sectionForHost:(NSString *)host;
- (NSString *)hostForSection:(NSInteger)section;

@end
