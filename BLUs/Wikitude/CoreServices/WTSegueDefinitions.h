#ifndef SDKExamples_WTSegueDefinitions_h
#define SDKExamples_WTSegueDefinitions_h

extern NSString * const kWTSegueIdentifier_CloseURLManagement;
extern NSString * const kWTSegueIdentifier_PresentURL_CompactSize;
extern NSString * const kWTSegueIdentifier_PresentURL_RegularSize;

extern NSString * const kWTSegueIdentifier_CancelURLCreation;
extern NSString * const kWTSegueIdentifier_SaveURL;

extern NSString * const kWTSegueIdentifier_PresentPoiDetails;

#endif
