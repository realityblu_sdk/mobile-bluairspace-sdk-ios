
import UIKit
import CoreLocation


protocol BLULocationManagerDelegate: class {
    func searchLoaction(didFindLocation loc: BLULocation?)
}


class BLULocationManager: NSObject, CLLocationManagerDelegate {
    
    @objc private class var sInstance: BLULocationManager {
        struct LocationSingleton {
            static let instance = BLULocationManager()
        }
        return LocationSingleton.instance
    }
    
    // the sharedInstance class method can be reached from ObjC
    @objc class func sInst() -> BLULocationManager {
        return BLULocationManager.sInstance
    }
    
    // lazy var geoCoder = CLGeocoder()
    weak var delegate: BLULocationManagerDelegate?
    private var definedLocation: BLULocation?
    private var onLocationDefined: ((BLULocation) -> Void)? //method to be called in some viewcontroller
    //var isLocadionAllowed = false
    
    
    lazy var locationManager: CLLocationManager? = {
        if Bundle.main.object(forInfoDictionaryKey: "NSLocationWhenInUseUsageDescription") != nil {
            var _locationManager = CLLocationManager()
            _locationManager.delegate = self
            _locationManager.requestWhenInUseAuthorization()
            return _locationManager
        } else {
            //            print("PRGLocationSearchBar Error: Location Services cannot be initialized. The app's Info.plist must contain an NSLocationWhenInUseUsageDescription key with a string value explaining to the user how the app uses this data")
            return nil
        }
    }()
    
    func searchLocation(locationDefined: ((BLULocation) -> Void)?) {
        self.onLocationDefined = locationDefined
        if areLocationPermissionsAvailable() {
            locationManager?.requestLocation()
        }
    }
    
    //    func getKnownLocation() -> BLULocation {
    //        return PrefsUtil.instance.getLastKnownLocation()
    //    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        geocode(withLocation: locations.last!, withSearchString: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        L.log(message: "LOCATION ERROR: \(error)", event: .error)
    }
    
    // MARK: - Geocoder Functionality
    private func geocode(withLocation location: CLLocation?, withSearchString searchString: String?){
        if location != nil {
            self.saveDefinedLocation(l: BLULocation(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude, address: nil))
            
            L.log(message: "Location defined: lat = \(String(describing: self.definedLocation?.latitude)), lon = \(String(describing: self.definedLocation?.longitude))", event: .info)
            self.delegate?.searchLoaction(didFindLocation: self.definedLocation)
            self.onLocationDefined?(self.definedLocation!)
           
        } else {
            L.log(message: "Location can't be defined", event: .warning)
            self.saveDefinedLocation(l: BLULocation(latitude: 0, longitude: 0, address: nil))
            self.delegate?.searchLoaction(didFindLocation: self.definedLocation)
            self.onLocationDefined?(self.definedLocation!)
        }
    }
    
    private func saveDefinedLocation(l: BLULocation) {
        self.definedLocation = BLULocation(latitude: l.latitude, longitude: l.longitude, address: l.address)
        PrefsUtil.instance.saveLastKnownLocation(loc: l)
    }
    
    // MARK: - Check for location permissions
    func areLocationPermissionsAvailable() -> Bool {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
           // L.log(message: "ALLOW LOCATION: authorizedWhenInUse", event: .info)
           // self.isLocadionAllowed = true
            return true
        case .notDetermined:
           // L.log(message: "ALLOW LOCATION: notDetermined", event: .info)
            locationManager?.requestWhenInUseAuthorization()
           // self.isLocadionAllowed = false
            return false
        case .restricted, .denied:
            
            L.log(message: "In order for us to be able to display markerbased AR depending on your location, please open this app's settings and set location access to 'When in use'.", event: .error)
//            let alertController = UIAlertController(
//                title: "Location Access Disabled",
//                message: "In order for us to be able to get your location, please open this app's settings and set location access to 'When in use'.",
//                preferredStyle: .alert)
//
//            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//            alertController.addAction(cancelAction)
//
//            let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
//                if let url = URL(string:UIApplicationOpenSettingsURLString) {
//                    UIApplication.shared.open(url , options: [:], completionHandler: nil)
//                }
//            }
//            alertController.addAction(openAction)
//
//            (UIApplication.shared.delegate as! AppDelegate).window!.rootViewController?.present(alertController, animated: true, completion: nil)

           // self.isLocadionAllowed = false
            return false
        case .authorizedAlways:
           // L.log(message: "ALLOW LOCATION: authorizedAlways", event: .info)
           // self.isLocadionAllowed = true
            return true
        }
    }
    
}

