import Foundation

@objc public class SwiftFromObjCBridge: NSObject {
    ///>>> The singleton instance
    // swiftSharedInstance is not accessible from ObjC
    @objc class var swiftSharedInstance: SwiftFromObjCBridge {
        struct Singleton {
            static let instance = SwiftFromObjCBridge()
        }
        return Singleton.instance
    }
    
    // the sharedInstance class method can be reached from ObjC
    @objc public class func sInst() -> SwiftFromObjCBridge {
        return SwiftFromObjCBridge.swiftSharedInstance
    }
    
    @objc public var onCachedUrlsReady: (() -> Void)?
    @objc public var onPersonalVideoDownloaded: (() -> Void)?
    @objc public var onNextFileLoaded: (() -> Void)?
    @objc public var onLoadingComplete: (() -> Void)?
    @objc public var onPathsArrayChanged: (() -> Void)?
    @objc public var arExperience: WTAugmentedRealityExperience? = nil
    @objc public var isMarkerlessAR: Bool = true;
    @objc public var arType: Int = -1; //0 = instant, 1 = scan, 2 = geo
    @objc public var licenseKey: String = "";
    @objc public var bluLicenseKey: String = "";
    var fakeLocations: FakeLocations? = nil
   

    @objc public var personalVideoDownloaded: String = "" {
        didSet {
            onPersonalVideoDownloaded?()
        }
    }
    @objc public var personalVideoDownloadedId: String = ""
    
    @objc public var scanExperiencesCached: String = "" {
        didSet {
            onCachedUrlsReady?()
        }
    }
    
    @objc public var allFilesLoaded: String = "" {
        didSet {
            onLoadingComplete?()
        }
    }
    
    @objc public var scanExperienceDownloaded: String = "" { //single url that will be immediatelly passed to JS
        didSet {
            onNextFileLoaded?()
        }
    }

    @objc public var experiencesPathsArrayString: String = "" {
        didSet {
            onPathsArrayChanged?()
        }
    }
    
    @objc public func activateMarkerLessExperienceDoNotShowTips() {
        PrefsUtil.instance.activateMarkerLessExperienceDoNotShowTips()
    }
    
    @objc public func getMarkerLessExperienceDoNotShowTipsActivated() -> Bool {
        return PrefsUtil.instance.getMarkerLessExperienceDoNotShowTipsActivated()
    }
    
    @objc public func getMarkerbasedClientToken() -> String {
        return PrefsUtil.instance.getMarkerbasedClientData().0
    }
    
    @objc public func getMarkerbasedCollectionId() -> String {
        return PrefsUtil.instance.getMarkerbasedClientData().1
    }
    
    @objc public func getMarkerbasedGroupId() -> String {
        return PrefsUtil.instance.getMarkerbasedClientData().2
    }
    
    @objc public func getProofingCollectionId() -> String {
        return PrefsUtil.instance.getMarkerbasedClientData().3
    }
    
    @objc public func getKnownLocation() -> [Double] {
        let loc = PrefsUtil.instance.getLastKnownLocation()
        return [loc.latitude, loc.longitude]
    }
    
    @objc public func getEmailUserSelfFromPref() -> String {
        return PrefsUtil.instance.getEmailUserSelf()
    }
    
    @objc public func getIsNeedMarkerBasedTutorial() -> Bool {
        return PrefsUtil.instance.getIsNeedMarkerBasedTutorial()
    }
    
    @objc public func setNeedMarkerBasedTutorial(value: Bool) {
        PrefsUtil.instance.setNeedMarkerBasedTutorial(value: value)
    }
    
    @objc public func getMarkerlessMarkerbasedUrl() -> String {
        return RESTUtil.instance.apiHandler.getMarkerlessMarkerbasedUrl()
    }
}

