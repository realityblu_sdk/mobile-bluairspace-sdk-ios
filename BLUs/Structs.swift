import Foundation


public struct UserInfo: Decodable {
    public var firstName: String
    public var secondName: String
    public var email: String
    public var businessName: String
    public var industry: String
    public var music: String
    public var gender: String
    
    public init() {
        firstName = ""
        secondName = ""
        email = ""
        businessName = ""
        gender = ""
        industry = UserIndustry.unspecified.string()
        music = UserMusic.unspecified.string()
    }
    public init(firstName: String,
                secondName: String,
                email: String,
                businessName: String,
                industry: String,
                music: String,
                gender: String) {
        
        self.firstName = firstName
        self.secondName = secondName
        self.email = email
        self.businessName = businessName
        self.industry = industry
        self.music = music
        self.gender = gender
    }
}

public struct FeedbackData {
    public var name: String
    public var email: String
    public var phone: String
    public var comment: String
    public var os: String
    public var appId: String
    public var captcha: String
    
    init() {
        name = ""
        email = ""
        phone = ""
        comment = ""
        os = ""
        appId = UserIndustry.unspecified.string()
        captcha = UserMusic.unspecified.string()
    }
    public init(name: String,
                email: String,
                phone: String,
                comment: String,
                os: String,
                appId: String,
                captcha: String) {
        
        self.name = name
        self.email = email
        self.phone = phone
        self.comment = comment
        self.os = os
        self.appId = appId
        self.captcha = captcha
    }
}

struct ARInitResponse: Decodable {
    public let error: Int?
    public let data: ARInitData?
    public let error_message: String?
}

struct BLULocation {
    let latitude: Double
    let longitude: Double
    let address: Dictionary<AnyHashable, Any>?
}

struct ARInitData: Decodable {
    public let client_token: String?
    public let wikitude_collection: String?
    public let proofing_wikitude_collection: String?
    public let ar_key: String?
    public let markerbased_js: String?
    public let version: Int?
    public let min_sdk_version: Int?
    public let group_id: String?
}

public struct MarkerbasedMarker: Decodable, Encodable {
    public let title: String
    public let icon: String
    public let target_file: String
}

public struct MarkerlessGroup: Decodable, Encodable {
    public let account_id: Int?
    public let id: Int?
    public let image: String?
    public let name: String?
}

internal struct MarkerlessExperienceGroup: Decodable {
    public let account_id: Int?
    public let group_id: Int?
    public let group_name: String?
    public let list_id: Int?
    public let list_name: String?
    public let experiences: [MarkerlessExperience]?
}

public struct MarkerbasedOptions {
    public var proofingEnabled: Bool
    public var singleScanEnabled: Bool
    
    public init() {
        proofingEnabled = false
        singleScanEnabled = false
    }
    
    public init(enableProofing proofing: Bool = false,
                enableSingleScan singleScan: Bool = false) {
        self.proofingEnabled = proofing
        self.singleScanEnabled = singleScan
    }
}

internal struct Transform {
    public let scale: Point3D
    public let position: Point3D
    public let rotation: Point3D
    
    init(fromComponents sc: Point3D = Point3D(fromComponents: 1, py: 1, pz: 1), pos: Point3D = Point3D(), rot: Point3D = Point3D()) {
        scale = sc
        position = pos
        rotation = rot
    }
}

internal struct FakeLocations {
    public let latitude: Double
    public let longitude: Double
    public let altitude: Double
    public let accuracy: Double
    
    public init(lat: Double, lon: Double, alt: Double = 0, acc: Double = 1) {
        latitude = lat
        longitude = lon
        altitude = alt
        accuracy = acc
    }
}

internal struct Point3D {
    public let x: Float
    public let y: Float
    public let z: Float
    
    init(fromComponents px: Float = 0, py: Float = 0, pz: Float = 0) {
        x = px
        y = py
        z = pz
    }
    
    init(fromArray xyz: [Float]) {
        x = xyz[0]
        y = xyz[1]
        z = xyz[2]
    }
}

public struct MarkerlessExperience: Decodable, Encodable, Equatable {
    public let id: Int?
    public let name: String?
    public let image: String?
    public let icon: String?
    public let description: String?
    public let file_name: String?
    public let shop_url: String?
    public let obj_animations: String?
    public var group: String?
    public var subgroup: String?
    public var audio_file: String?
    public var audio_loop: Int?
    public var scale: [Float]? = [1, 1, 1]
    public var position: [Float]? = [0, 0, 0]
    public var rotation: [Float]? = [0, 0, 0]
    public var scale_mobile: Float? = 0.01
    
    public static func == (lhs: MarkerlessExperience, rhs: MarkerlessExperience) -> Bool {
        return lhs.id == rhs.id &&
            lhs.name == rhs.name &&
            lhs.image == rhs.image &&
            lhs.file_name == rhs.file_name
    }
}

struct MExperienceAudio {
    public let audioFile: String
    public let audioLoop: Int
    
    init(audio: String?, loop: Int?) {
        self.audioFile = audio ?? ""
        audioLoop = loop ?? 0
    }
}


public struct ARContact {
    public let name: String
    public var nameCompany: String?
    public var title: String?
    public var phone: String?
    public var phoneType: String?
    public var email: String?
    public var emailType: String?
    
    init(name: String) {
        self.name = name
        nameCompany = nil
        title = nil
        phone = nil
        phoneType = nil
        email = nil
        emailType = nil
    }
}


struct Config {
    // This is private because the use of 'appConfiguration' is preferred.
    private static let isTestFlight: Bool = Bundle.main.appStoreReceiptURL?.path.contains("sandboxReceipt") ?? false
    
    // This can be used to add debug statements.
    static var isDebug: Bool {
        #if DEBUG
        return true
        #else
        return false
        #endif
    }
    
    static var appConfiguration: AppConfiguration {
        if isDebug {
            return .Debug
        } else if isTestFlight {
            return .TestFlight
        } else {
            return .AppStore
        }
    }
}


