import Foundation

@objc public class MarkerbasedDownloadUtililty: NSObject {
    
    let KEY_MODELS = "models"
    let KEY_BTNS = "buttons"
    let KEY_MARKERS = "markers"
    let KEY_IMAGES = "images"
    
    struct ScanDownloadableItem {
        let id: Any
        let name: String
        let url: String
    }
    
    @objc class var swiftSharedInstance: MarkerbasedDownloadUtililty {
        struct MBSingleton {
            static let instance = MarkerbasedDownloadUtililty()
        }
        return MBSingleton.instance
    }
    
    // the sharedInstance class method can be reached from ObjC
    @objc public class func sharedInstance() -> MarkerbasedDownloadUtililty {
        return MarkerbasedDownloadUtililty.swiftSharedInstance
    }
    
    var isDownloadingExperiences: Bool = false;
    var currentDownloadTypeIndex = 0
    var currentDownloadUrlIndex = 0
    var currentDownloadPersonalVideoIndex = 0
    var filesDownloaded: Int = 0
    var markerId: Int = 0
    
    var jsonScene = JSON()
    var urlsDict = [String: [ScanDownloadableItem]]()
    var urlKeys = [String]() //used for urlDict iteration in oredered sequence
    var localFileUrlsArray = [String]()
    var previouslyDownloadedFileLocalUrls = [String: [ScanDownloadableItem]]()
    var urlsToDownload = [(String, Any, String, String, String, DownloadRequest.DownloadFileDestination)]() //(type, uniqueId, name, urltoDownlod, existingFilePath, destination)
    var personalVideos = [(Int, String, Int)]()
    
    var modelUrls = [ScanDownloadableItem]()
    var btnUrls = [ScanDownloadableItem]()
    var imagesUrls = [ScanDownloadableItem]()
    var experienceId: Int = -1
    
    
    var onDownloadCancel: (() -> Void)? //method to be called in some viewcontroller
    
    
    @objc public func doDownloadPersonalVideo(dataJson: [String: Any]) {
        let jsonObject = JSON(dataJson)
        
        if let theJSONData = try? JSONSerialization.data( withJSONObject: dataJson, options: .prettyPrinted),
            let theJSONText = String(data: theJSONData, encoding: String.Encoding.ascii) {
        }
        
        if let videosArray = jsonObject["personal_videos"].array,
            let experienceId = jsonObject["experience_id"].int {
            for v in videosArray {
                if let idInt = v["id"].int, let storyboardId = v["storyboardId"].int {
                    self.personalVideos.append((experienceId, String(idInt), storyboardId))
                    L.log(message: "personalVideos=\(idInt)", event: .info)
                } else if let idString = v["id"].string, let storyboardId = v["storyboardId"].int{
                   self.personalVideos.append((experienceId, idString, storyboardId))
                    L.log(message: "personalVideos=\(idString)", event: .info)
                } else{
                     L.log(message: "Loading personal video failed: id or storyboardId does not exist", event: .warning)
                }
            }
        } else {
            L.log(message: "Loading personal video failed: error parsing personal_videos array", event: .warning)
        }
        
        startDownloadingPersonalVideosSequentially()
    }
    
    private func startDownloadingPersonalVideosSequentially() {
        guard personalVideos.count > 0 else {
            L.log(message: "Loading personal video failed: no personal videos", event: .warning)
            return
        }
        
        RESTUtil.instance.getUserPersonalVideo(expId: personalVideos[currentDownloadPersonalVideoIndex].0, storyboardId: personalVideos[currentDownloadPersonalVideoIndex].2, onSuccess: { (urlString) in
            
            let videoId = self.personalVideos[self.currentDownloadPersonalVideoIndex].1
            
            self.currentDownloadPersonalVideoIndex += 1
            SwiftFromObjCBridge.sInst().personalVideoDownloadedId = ""+videoId
            SwiftFromObjCBridge.sInst().personalVideoDownloaded = urlString
            
            if self.personalVideos.count > self.currentDownloadPersonalVideoIndex {
                self.startDownloadingPersonalVideosSequentially()
            }
        }) {msg in
            L.log(message: "Loading personal video failed: \(msg)", event: .warning)
        }
    }
    
    
    @objc public func doDownloadData(dataJson: [String: Any], onCancelDownload: @escaping (() -> Void)) {
        onDownloadCancel = onCancelDownload
        jsonScene = JSON(dataJson)
        
        let marker = jsonScene[KEY_MARKERS].array![0]
        if let item = marker["item"].dictionary {
            if let id = item["marker_id"]?.int {
                markerId = id
            }else if let id1 = item["serverID"]?.int {
                markerId = id1
            }else{
                L.log(message: "Unknown marker", event: .warning)
            }
        }
        
        
        btnUrls = getUrlsFromItemsArray(arrayKey: KEY_BTNS)
        modelUrls = getUrlsFromItemsArray(arrayKey: KEY_MODELS)
        imagesUrls = getUrlsFromItemsArray(arrayKey: KEY_IMAGES)
        
        if btnUrls.isEmpty && modelUrls.isEmpty && imagesUrls.isEmpty {
            L.log(message: "No file urls to download", event: .warning)
            handleLoadingComplete()
            return
        }
        
        experienceId = jsonScene["expId"].intValue
        if experienceId == 0 { experienceId = -1 }
        
        let snapped = jsonScene["snapped"].boolValue
        if (!snapped){
            AnalyticsUtil.faSendMarkerbasedRecognized(markerId: markerId, expId: experienceId)//should call this when new marker recognized AND should download data or get from cache. E.g when matker scanned 1-st time or second, third, etc. time after another marker.
        }
        
        
        if isDownloadingExperiences {
            L.log(message: "download is in progress, aborting", event: .warning)
            return
        }
        
        resetAllDataBeforeDownloading()
        
        urlsDict[urlKeys[0]] = btnUrls
        urlsDict[urlKeys[1]] = imagesUrls
        urlsDict[urlKeys[2]] = modelUrls
        
        extractUrls()
        sendPreviouslyDownloadedUrlsToScene()
        
        if !urlsToDownload.isEmpty {
            downloadDataInSequentially()
        } else {
            handleLoadingComplete()
        }
    }
    
    private func resetAllDataBeforeDownloading() {
        urlKeys = [KEY_BTNS, KEY_IMAGES, KEY_MODELS]
        urlsDict.removeAll()
        localFileUrlsArray.removeAll()
        previouslyDownloadedFileLocalUrls.removeAll()
        currentDownloadUrlIndex = 0
        currentDownloadTypeIndex = 0
        filesDownloaded = 0
        currentDownloadPersonalVideoIndex = 0
        personalVideos.removeAll()
    }
    
    private func extractUrls() {
        let fm = FileManager.default
        let fileDir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let documentsURL:NSURL = fm.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
        
        L.log(message: "extractUrls: fileDir: \(fileDir) +++ documentsUrl: \(documentsURL.absoluteString)", event: .info)
        
        let fileDirUrl = URL(fileURLWithPath: fileDir)
        
        for (type, downloadableItem) in urlsDict {
            var tempItems = [ScanDownloadableItem]()
            
            for dItem in downloadableItem {
                let urltoDownlod = dItem.url.replacingOccurrences(of: ".fbx", with: ".wt3", options: .caseInsensitive, range: nil)
                let fileName = removeJSRejectableChars(value: urltoDownlod.components(separatedBy: "/").last!)
                
                let existingFilePath = fileDirUrl.appendingPathComponent("\(fileName)").path
                if fm.fileExists(atPath: existingFilePath) {
                    L.log(message: "file \(existingFilePath) is already cached", event: .info)
                    tempItems.append(ScanDownloadableItem(id: dItem.id, name: dItem.name, url: existingFilePath))
                } else {
                    let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                        let fileURL = documentsURL.appendingPathComponent("\(fileName)")
                        return (fileURL!,[.removePreviousFile, .createIntermediateDirectories])
                    }
                    urlsToDownload.append((type, dItem.id, dItem.name, urltoDownlod, existingFilePath, destination))
                }
            }
            
            previouslyDownloadedFileLocalUrls[type] = tempItems
        }
    }
    
    private func sendPreviouslyDownloadedUrlsToScene() {
        var resultArray = [[String: Any]]()
        for (key, downloadableItem) in previouslyDownloadedFileLocalUrls {
            for u in downloadableItem {
                if let r = createJsonForSingleDownloadedFile(fileType: key, uniqueId: u.id, name: u.name, filePath: u.url) {
                    resultArray.append(r)
                }
            }
        }
        let jsonOutput: JSON = JSON(["localItems": resultArray])
        
        if let stringResult = jsonOutput.rawString(options: []) {
            SwiftFromObjCBridge.sInst().scanExperiencesCached = stringResult
        } else {
            L.log(message: "Error converting cached paths to JSON", event: .error)
        }
    }
    
    private func sendNextDownloadedUrlToScene(fileType: String, uniqueId: Any, name: String, filePath: String) {
        if let r = createJsonForSingleDownloadedFile(fileType: fileType, uniqueId: uniqueId, name: name, filePath: filePath) {
            
            let jsonOutput: JSON = JSON(["loadedItem": r])
            
            if let stringResult = jsonOutput.rawString(options: []) {
                SwiftFromObjCBridge.sInst().scanExperienceDownloaded = stringResult
            } else {
                L.log(message: "Error converting loaded path to JSON", event: .error)
            }
        }
    }
    
    func getUrlsFromItemsArray(arrayKey: String) -> [ScanDownloadableItem] {
        var resultArray = [ScanDownloadableItem]()
        if let itemsArray = jsonScene[arrayKey].array {
            for item in itemsArray {
                if let url = item["url"].string{
                    
                    let uniqueIdNumber = item["uniqueID"].int
                    let uniqueIdString = item["uniqueID"].string
                   
                    var uniqueId: Any? = nil
                    if (uniqueIdNumber != nil){
                        uniqueId = uniqueIdNumber
                    } else {
                        uniqueId = uniqueIdString
                    }
                    
                    // L.log(message: "downloading scan data, uniqueID: \(uniqueId), name: \(name), URL: \(url)", event: .info)
                    if (!url.isEmpty) {
                        let itemJson = item["item"]
                        if let name = itemJson["name"].string {
                            let itt = ScanDownloadableItem(id: uniqueId, name: name, url: url)
                            L.log(message: "downloading scan data, uniqueID: \(uniqueId), name: \(name), URL: \(url)", event: .info)
                            resultArray.append(itt)
                        }
                    }
                }
            }
        }
        return resultArray
    }
    
    func setFilePathsForItemsArray(arrayKey: String, filePathsArray: [String]) {
        if let itemsArray = jsonScene[arrayKey].array {
            
            var i = 0;
            for item in itemsArray {
                if item["file_path"].string != nil {
                    //filepath already exist, go to next item
                } else {
                    //need to download item and set filepath
                    jsonScene[arrayKey][i]["file_path"].string = filePathsArray[i]
                }
                i += 1
            }
        }
    }
    
    func createJsonForSingleDownloadedFile(fileType: String,
                                           uniqueId: Any,
                                           name: String,
                                           filePath: String,
                                           filePath2: String = "",
                                           filePath3: String = "") -> [String: Any]? {
        //let name = filePath.components(separatedBy: "/").last!;
        let data = JSON(["uniqueID": uniqueId, "name": name, "file_path_1": filePath, "file_path_2": filePath2, "file_path_3": filePath3])
        let result = JSON(["type": fileType, "data": data])
        return result.dictionary
    }
    
    func downloadDataInSequentially() {
        if urlsToDownload.isEmpty {
            handleLoadingComplete()
            AnalyticsUtil.faSendMarkerbasedClick(markerId: markerId, expId: experienceId) //send it from here because it means that previously this method DID download something
            return
        }
        //(type, uniqueId, name, urltoDownlod, existingFilePath, destination)
        let t = urlsToDownload[currentDownloadUrlIndex].0  //TODO: currentDownloadUrlIndex is always 0?
        let uniqueId = urlsToDownload[currentDownloadUrlIndex].1
        let itemName = urlsToDownload[currentDownloadUrlIndex].2
        let urlString = urlsToDownload[currentDownloadUrlIndex].3
        let existingFilePath = urlsToDownload[currentDownloadUrlIndex].4
        let destination = urlsToDownload[currentDownloadUrlIndex].5
        
        let fm = FileManager.default
        if fm.fileExists(atPath: existingFilePath) {
            L.log(message: "file \(existingFilePath) is already cached >>> ", event: .info)
            self.sendNextDownloadedUrlToScene(fileType: t, uniqueId: uniqueId, name: itemName, filePath: existingFilePath)
            self.handleDownloadedData()
        } else {
            L.log(message: "downloadDataInSequentialy: file \(existingFilePath) downloading... files rest: \(urlsToDownload.count)", event: .info)
            SessionManager.default.download(urlString, to: destination).downloadProgress(closure: { (prog) in
                //display progress
            }).response { response in
                self.filesDownloaded += 1
                if response.error == nil, let filePath = response.destinationURL?.path {
                    self.sendNextDownloadedUrlToScene(fileType: t, uniqueId: uniqueId, name: itemName, filePath: filePath)
                    self.handleDownloadedData()
                } else {
                    L.log(message: "error downloading file \(existingFilePath), \(response.error.debugDescription)", event: .warning)
                    self.handleDownloadedData()
                }
            }
        }
    }
    
    func removeJSRejectableChars(value: String) -> String {
        //wikitude js file don't like some unicode characters. We just use their unicode ids without % sign for local names and replace + signs
        return value.replacingOccurrences(of: "%", with: "")
            .replacingOccurrences(of: "\"", with: "")
            .replacingOccurrences(of: "\'", with: "")
            .replacingOccurrences(of: "+", with: "_")
    }
    
    func handleDownloadedData() {
        if !urlsToDownload.isEmpty {
            urlsToDownload.removeFirst()
        }
        downloadDataInSequentially()
    }
    
    func handleLoadingComplete() {
        L.log(message: "downloadDataInSequentialy - nothing to download, all files were downloaded", event: .info)
        SwiftFromObjCBridge.sInst().allFilesLoaded = "all"
    }
    
    func cancelDownloading() {
        let sessionManager = SessionManager.default;
        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach { $0.cancel() }
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel() }
            
            self.isDownloadingExperiences = false
            
            DispatchQueue.main.async(execute: {() -> Void in
                self.onDownloadCancel?()
            })
        }
    }
}

