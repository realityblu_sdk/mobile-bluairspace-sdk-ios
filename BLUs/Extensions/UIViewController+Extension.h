//
//  UIViewController+Extension.h
//  BLUs
//
//  Created by jegor on 10/31/19.
//  Copyright © 2019 Den Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (Extension)

- (BOOL)isModal;

@end

NS_ASSUME_NONNULL_END
