//
//  UIViewController+Extension.m
//  BLUs
//
//  Created by jegor on 10/31/19.
//  Copyright © 2019 Den Alex. All rights reserved.
//

#import "UIViewController+Extension.h"


@implementation UIViewController (Extension)

- (BOOL)isModal {
    if([self presentingViewController])
        return YES;
    if([[[self navigationController] presentingViewController] presentedViewController] == [self navigationController])
        return YES;
    if([[[self tabBarController] presentingViewController] isKindOfClass:[UITabBarController class]])
        return YES;

   return NO;
}

@end
