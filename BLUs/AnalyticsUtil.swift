import Foundation
import FirebaseCore
import FirebaseAnalytics

class AnalyticsUtil {
    
    //COMMON
    private static let A_SESSION_DURATION = "ar_session_dur"
    //SCAN
    private static let A_MARKERBASED_CLICK = "ar_mb_click"
    private static let A_MARKERBASED_RECOGNISED = "ar_mb_recognized"
    private static let A_MARKERBASED_EXP_DURATION = "ar_mb_exp_duration"
    private static let A_MARKERBASED_EXP_ACTION = "ar_mb_exp_action"
    private static let A_MARKERBASED_PROOFING = "ar_mb_proofing"
    
    //MARKERLESS
    private static let A_MARKERLESS_CLICK = "ar_ml_click"
    private static let A_MARKERLESS_MODELS_USED = "ar_ml_models_used"
    private static let A_MARKERLESS_EXP_DURATION = "ar_ml_exp_duration"
    
    //PARAMS
    private static let A_PARAM_MARKER_ID = "marker_id"
    private static let A_PARAM_ACCOUNT_ID = "account_id"
    private static let A_PARAM_EXPERIENCE_ID = "experience_id"
    private static let A_PARAM_DURATION = "duration"
    private static let A_PARAM_AR_TYPE = "ar_type"
    private static let A_PARAM_DEVICE_ID = "device_id"
    private static let A_PARAM_LOCATION = "location"
    
    private static let A_PARAM_EXP_ACTION_TYPE = "exp_action_type"
    private static let A_PARAM_EXP_ACTION_ID = "exp_action_id"
    private static let A_PARAM_EXP_ACTION_NAME = "exp_action_name"
    private static let A_PARAM_EXP_ACTION_VALUE = "exp_action_value"

    
    //USER PROPERTIES
    private static let U_ENVIRONMENT = "user_environment"
    public static var isProofing: Bool = false
    
    public static func setEnvironment(EnvironmentName: String){
        Analytics.setUserProperty(EnvironmentName, forName: U_ENVIRONMENT)
    }
    
    
    //SCAN
    public static func faSendMarkerbasedClick(markerId: Int, expId: Int) {
        //need acc/instance/campaign   has marker id and experience id
        let par = [A_MARKERBASED_PROOFING: isProofing, A_PARAM_MARKER_ID: markerId, A_PARAM_EXPERIENCE_ID: expId] as [String : Any]
        Analytics.logEvent(A_MARKERBASED_CLICK, parameters: par)
    }

    public static func faSendMarkerbasedRecognized(markerId: Int, expId: Int) {
        //need acc/instance/campaign  has marker id and experience id
        let deviceId = ATools.instance.getDeviceId()
        let lastLoc = PrefsUtil.instance.getLastKnownLocation()
        
        Analytics.logEvent(A_MARKERBASED_RECOGNISED, parameters: [
            A_MARKERBASED_PROOFING: isProofing,
            A_PARAM_MARKER_ID: markerId,
            A_PARAM_EXPERIENCE_ID: expId,
            A_PARAM_DEVICE_ID: deviceId,
            A_PARAM_LOCATION: "\(lastLoc.latitude),\(lastLoc.longitude)"])
    }
    
    public static func faSendMarkerbasedExpDuration(expId: Int, duration: Int) {
        let deviceId = ATools.instance.getDeviceId()
        let lastLoc = PrefsUtil.instance.getLastKnownLocation()
        
        Analytics.logEvent(A_MARKERBASED_EXP_DURATION, parameters: [
            A_MARKERBASED_PROOFING: isProofing,
            A_PARAM_EXPERIENCE_ID: expId,
            A_PARAM_DURATION: duration,
            A_PARAM_DEVICE_ID: deviceId,
            A_PARAM_LOCATION: "\(lastLoc.latitude),\(lastLoc.longitude)"])
    }
    
    public static func faSendMarkerbasedExpAction(expId: Int, type: Int, contentId: Any, name: Any, value: Any) {
        Analytics.logEvent(A_MARKERBASED_EXP_ACTION, parameters: [
            A_MARKERBASED_PROOFING: isProofing,
            A_PARAM_EXPERIENCE_ID: expId,
            A_PARAM_EXP_ACTION_TYPE: type,
            A_PARAM_EXP_ACTION_ID: contentId,
            A_PARAM_EXP_ACTION_NAME: name,
            A_PARAM_EXP_ACTION_VALUE: value])
    }
    
    //MARKERLESS
    public static func faSendMarkerlessClick(accountId: Int) {
        //need acc/instance/campaign, has account id and experience id
        Analytics.logEvent(A_MARKERLESS_CLICK, parameters: [A_PARAM_ACCOUNT_ID: accountId])
    }
    
    public static func faSendMarkerlessModelsUsed(experienceIdsArray: [Int]) {
        //need acc/instance/campaign, has account id and experience id
        
        
        let deviceId = ATools.instance.getDeviceId()
        let lastLoc = PrefsUtil.instance.getLastKnownLocation()
        
        var parameters: [String: Any] = [String: Any]()
        var i = 0;
        for id in experienceIdsArray {
            parameters["experience_\(i)"] = id
            i = i + 1
        }
        
        parameters[A_PARAM_DEVICE_ID] = deviceId
        parameters[A_PARAM_LOCATION] =  "\(lastLoc.latitude),\(lastLoc.longitude)"
        
        Analytics.logEvent(A_MARKERLESS_MODELS_USED, parameters: parameters)
    }
    
    public static func faSendMarkerlessSessionDuration(arType: Int, durationSeconds: Int) {
        Analytics.logEvent(A_SESSION_DURATION, parameters: [
            A_MARKERBASED_PROOFING: isProofing,
            A_PARAM_AR_TYPE: arType,
            A_PARAM_DURATION: durationSeconds])
    }
    
    
    public static func faSendMarkerlessExpDuration(ExpIdWithDurationList: [Int: Int] ) {
        let deviceId = ATools.instance.getDeviceId()
        let lastLoc = PrefsUtil.instance.getLastKnownLocation()
        
        
        var parameters: [String: Any] = [String: Any]()
        var i = 0;
       
        for (e, value) in ExpIdWithDurationList  {
            parameters["experience_\(i)"] = e
            parameters["duration_\(i)"] = value
            i = i + 1
        }
        
        parameters[A_PARAM_DEVICE_ID] = deviceId
        parameters[A_PARAM_LOCATION] =  "\(lastLoc.latitude),\(lastLoc.longitude)"
        
         Analytics.logEvent(A_MARKERLESS_EXP_DURATION, parameters: parameters)
        
    }
    
 
}
