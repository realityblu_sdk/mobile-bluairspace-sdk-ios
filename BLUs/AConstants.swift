

import Foundation

struct AConstants {
    static let CURRENT_BLU_SDK_VERSION: Int = 2
    
    static func getOutdatedSDKMessage() -> String {
        return "BLU SDK is outdated. Current version is \(CURRENT_BLU_SDK_VERSION). Minimal SDK version is \(PrefsUtil.instance.getMinSDKInitVersion()). Please update the SDK to continue using it."
    }
  
//    static let IMG_PLACEHOLDER_WIKITUDE = "wikitude_placeholder_white"
//    static let WT3_PLACEHOLDER_WIKITUDE = "wikitude_placeholder_wt3"
    
    static let MARKERLESS_EXPERIENCE_SELECTED_COUNT_MAX = 5
    static let MARKERBASED_EXPERIENCE_INITIAL_JS_VERSION = 999
    
    static let IMG_PLACEHOLDER_AR = "ar_placeholder_white"
    static let WT3_PLACEHOLDER_AR = "placeholder_wt3"
    static let PREFS_KEY_LAST_LOCATION_LT = "blu_last_location_lt"
    static let PREFS_KEY_LAST_LOCATION_LN = "blu_last_location_ln"
    static let PREFS_KEY_CURRENT_BLU_SDK_VERSION = "blu_sdk_version"
    static let PREFS_KEY_MARKERBASED_JS_VERSION = "markerbased_js_version"
    static let PREFS_KEY_MARKERLESS_DO_NOT_SHOW_TIPS = "markerless_dont_show_tips"
    static let PREFS_KEY_INSTALL_SETTINGS = "install_settings"
    static let PREFS_KEY_ENVIRONMENT_TYPE = "env_type"
    static let PREFS_KEY_MARKERBASED_CLIENT_TOKEN = "client_token"
    static let PREFS_KEY_APP_VERSION = "app_version"
    static let PREFS_KEY_MARKERBASED_COLLECTION_ID = "collection_id"
    static let PREFS_KEY_MARKERBASED_PROOFING_COLLECTION_ID = "proofing_collection_id"
    static let PREFS_KEY_MARKERBASED_GROUP_ID = "group_id"
    static let PREFS_KEY_LAUNCH_TIME = "app_launch_time"
    static let PREFS_KEY_USER_FIRST_NAME = "user_first_name"
    static let PREFS_KEY_USER_FAMILY_NAME = "user_family_name"
    static let PREFS_KEY_USER_BUSINESS_NAME = "user_b_name"
    static let PREFS_KEY_USER_EMAIL = "user_email"
    static let PREFS_KEY_USER_MUSIC = "user_music"
    static let PREFS_KEY_USER_GENDER = "user_gender"
    static let PREFS_KEY_USER_HEALTH = "user_health"
    static let PREFS_KEY_USER_INDUSTRY = "user_industry"
    static let PREFS_KEY_ANALYTICS = "analytics_enable"
    
    static let PREFS_KEY_USER_EMAIL_SELF = "user_email_self"
    static let PREFS_KEY_IS_NEED_MARKERBASED_TUTORIAL = "is_need_markerbased_tutorial"
    
    static let MESSAGE_AR_INIT_IN_PROGRESS = "AR initialization is in progress, please wait..."
    
    static let ERROR_MESSAGE_DOWNLOADING_AMOUNT = "Invalid amount of downloading experiences"
    static let ERROR_MESSAGE_DOWNLOADING_USER_CANCEL = "Downloading cancelled by user"
    static let ERROR_MESSAGE_CAMERA_ACCESS = "You need to provide camera access in order to open AR controller"
    static let ERROR_MESSAGE_SIMULATOR_USAGE = "Unfortunately you can't present AR controller on simulator"
    
    static let SID_QR_SCANNER = "qr_scanner"
}
