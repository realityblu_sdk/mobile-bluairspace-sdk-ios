import Foundation
import MessageUI
import EventKit
import Contacts
import ContactsUI
import SafariServices

protocol ProofingControllerDelegate: class {
    func startCaptureSession()
}

internal class ARBaseViewController: WTAugmentedRealityViewController, MFMailComposeViewControllerDelegate, CNContactViewControllerDelegate, BLUARController {
    
    public var options: MarkerbasedOptions?

    internal var useCustomSpinner = false
    internal var proofingCode: String?
    
    @objc internal var sessionStartTime: Date = Date()
    internal var isScanned = false
    internal var isScanCancelled = false
    
    //  Play Models needed for analytics
    internal var playModels: [Int32: Date] = [Int32 : Date]()
    //  Dates and expId needed for analytics
    internal var startDate: Date! = nil
    internal var endDate: Date! = nil
    internal var expId: Int = -1

    
    internal weak var proofingDelegate: ProofingControllerDelegate?
    
    class func instantiate() -> ARBaseViewController {
        let storyboard = UIStoryboard(name: "BLUControllers", bundle: Bundle(for: ARBaseViewController.self))
        return storyboard.instantiateViewController(withIdentifier: "arController") as! ARBaseViewController
    }
    
    deinit {
        L.debug(message: "deinit AR controller")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        if let floc = SwiftFromObjCBridge.sInst().fakeLocations {
            useFakeLocations(latitude: floc.latitude, longitude: floc.longitude, altitude: floc.altitude, accuracy: floc.accuracy)
        } else {
            stopUsingFakeLocation()
        }
        architectView.clearCache()
        
        let app = UIApplication.shared
        //Register for the applicationWillResignActive anywhere in your app.
        NotificationCenter.default.addObserver(self, selector: #selector(ARBaseViewController.applicationWillResignActive(notification:)), name: NSNotification.Name.UIApplicationWillResignActive, object: app)
        AnalyticsUtil.isProofing = self.isProofing
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.sessionStartTime = Date()
        self.sendResumeEvent()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.sendPauseEvent()
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.sendMarkerlessDurationAnalytics()
        self.sendPlayAnalitycs()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    private func useFakeLocations(latitude: CLLocationDegrees, longitude: CLLocationDegrees, altitude: CLLocationDistance = 0, accuracy: CLLocationAccuracy = 1) {
        if (altitude == 0) {
            architectView.injectLocation(withLatitude: latitude, longitude: longitude, accuracy: accuracy)
        } else {
            architectView.injectLocation(withLatitude: latitude, longitude: longitude, altitude: altitude, accuracy: accuracy)
        }
        
        L.log(message: "USING FAKE LOCATIONS: [\(latitude),\(longitude)], ALT: \(altitude), ACC: \(accuracy)", event: .warning)
        architectView.setUseInjectedLocation(true)
    }
    
    private func stopUsingFakeLocation() {
        if (architectView.isUsingInjectedLocation()) {
            architectView.setUseInjectedLocation(false)
            L.log(message: "USING FAKE LOCATIONS STOPPED", event: .warning)
        }
    }
    
    private func sendPlayAnalitycs() {
        let arType = SwiftFromObjCBridge.sInst().arType
        if (arType == 0){
            self.modelPlayStopAll()
        } else if(arType == 1) {
            self.experiencePlayStop()
        }
    }
    
    open override func onMarkerRecognized(_ markerId: Int32) {
        super.onMarkerRecognized(markerId)
    }
    
    func enableProofing(withCode code: String) {
        architectView.callJavaScript("NativeBridge.enableProofing('\(code)')")
    }
    
    final override public func onReceivedActionDismiss() {
        if self.proofingDelegate != nil {
            showScanningDialog()
        } else {
            dismissBasic()
        }
    }
    
    final func dismissProofing() {
        self.proofingDelegate?.startCaptureSession()
        if self.isModal {
            self.dismissModal()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    final func dismissBasic() {
        if self.isModal {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    final override public func onReceivedButtonActionPhone(_ phone: String?) {
        phone?.makeACall()
    }
    
    final override public func onReceivedButtonActionOpenExternalUrl(_ urlString: String?) {
        guard
            let _urlString = urlString,
            let url = URL(string: _urlString)
        else { print("BLU: Unable to open \(String(describing: urlString))"); return }
        let safariVC = SFSafariViewController(url: url)
        self.present(safariVC, animated: true, completion: nil)
    }

    final override public func onReceivedButtonActionEmail(_ email: String?, subject: String?) {
        guard let _email = email else {
            L.log(message: "Error sending email: Email is empty", event: .warning)
            return
        }
        
        let mailController = MFMailComposeViewController()
        mailController.mailComposeDelegate = self
        mailController.setToRecipients([_email])
        mailController.setSubject((subject != nil) ? subject! : "Hello RealityBLU")
        mailController.setMessageBody("Your app is great!", isHTML: false)
        
        if MFMailComposeViewController.canSendMail() {
            self.present(mailController, animated: true, completion: nil)
        } else {
            L.log(message: "Error sending email to \(email!)", event: .warning)
        }
    }

    func mailComposeController(_ controller: MFMailComposeViewController,
                                      didFinishWith result: MFMailComposeResult,
                                      error: Error?) {
        L.debug(message: "dismiss get called")
        controller.dismiss(animated: true, completion: nil)
    }
    
    final override public func onReceivedButtonActionCalendar(withTitle theTitle: String?, _ startDate: String?, _ endDate: String?, _ description: String?) {
        let eventStore: EKEventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: {(granted, error) in
            if granted && error == nil {
                let event: EKEvent = EKEvent(eventStore: eventStore)
                
                let eTitle = theTitle ?? "BLUairspace AR event"
                let eNote = description ?? ""
                let eStart = ATools.instance.parseDateIso8601(dateString: startDate)
                let eEnd = ATools.instance.parseDateIso8601(dateString: endDate)
                
                event.title = eTitle
                event.startDate = eStart
                event.endDate = eEnd
                event.notes = eNote
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                    
                    let alertController = UIAlertController(title: "Event added to calendar", message: nil, preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    DispatchQueue.main.async {
                        self.present(alertController, animated: true, completion: nil);
                    }
                } catch let error as NSError {
                    L.log(message: "Error saving calendar event: \(String(describing: error))", event: .warning)
                    let alertController = UIAlertController(title: "Error saving event", message: error.localizedDescription, preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    DispatchQueue.main.async {
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            } else if (!granted && error == nil) {
                let alertController = UIAlertController(title: "Error saving event", message: "Please, allow your app using calendar", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))

                alertController.addAction(UIAlertAction(title: "Settings", style: .default) { (action) in
                    UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                })
                DispatchQueue.main.async {
                    self.present(alertController, animated: true, completion: nil)
                }
            } else {
                L.log(message: "Error creating calendar event: \(String(describing: error))", event: .warning)
            }
        })
    }
    
    final override public func onReceivedButtonActionVCard(withName name: String?, _ company: String?, _ title: String?, _ phoneType: String?, _ phone: String?, _ emailType: String?, _ email: String?) {
        if let n = name {
           
            var c = ARContact(name: n)
            c.nameCompany = company
            c.title = title
            c.phoneType = phoneType
            c.phone = phone
            c.emailType = emailType
            c.email = email
            addContact(contact: c)
        } else {
            L.log(message: "Unable to create vcard, name is nil", event: .warning)
        }
    }
    
    func addContact(contact: ARContact?) {
        guard let c = contact else {
            onErrorAddingContact()
            return
        }
        let con = CNMutableContact()
        con.givenName = c.name
        if let title = c.title {
            con.jobTitle = title
        }
        if let nameCompany = c.nameCompany {
            con.organizationName = nameCompany
        }
        if let phoneLabel = c.phoneType {
            if let phone = c.phone {
                con.phoneNumbers.append(CNLabeledValue(label: phoneLabel, value: CNPhoneNumber(stringValue: phone)))
            }
        }
        if let emailLabel = c.emailType {
            if let email = c.email {
                let em = CNLabeledValue(label: emailLabel, value: email as NSString)
                con.emailAddresses = [em]
            }
        }
        
        let unkvc = CNContactViewController(forUnknownContact: con)
        //unkvc.message = "Adding from AR Scene"
        
        unkvc.contactStore = CNContactStore()
        unkvc.delegate = self
        unkvc.allowsActions = false

        let navigationController = UINavigationController(rootViewController: unkvc)
        navigationController.isNavigationBarHidden = false
        self.present(navigationController, animated: true) {
            unkvc.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .done, target: self, action: #selector(self.createContactViewControllerCancelEvent))
            unkvc.navigationController?.setNavigationBarHidden(false, animated: false)
        }
    }
    
    @objc open func createContactViewControllerCancelEvent() {
        self.dismiss(animated: true, completion: nil)
    }
    
    open func onErrorAddingContact() { }
    
    final internal func flashlightNative() {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }
        
        do {
            try device.lockForConfiguration()
            
            if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                device.torchMode = AVCaptureDevice.TorchMode.off
            } else {
                do {
                    try device.setTorchModeOn(level: 1.0)
                } catch {
                    print(error)
                }
            }
            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
    
    @objc private func sendAnalyticsAction(_ expId: Int32, _ type: Int32, _ contentId: Any?, _ name: Any?, _ value: Any?) {
        AnalyticsUtil.faSendMarkerbasedExpAction(expId: Int(expId), type: Int(type), contentId: contentId!, name: name!, value: value!)
    }
    
    @objc private func experiencePlayStart(_ expId: Int32) {
        L.log(message: "MarkerBased input expId=\(expId)", event: .info)
        if (startDate != nil) { return }
        
        self.expId = Int(expId)
        startDate = Date()
    }
    
    @objc private func experiencePlayStop() {
        if (startDate == nil) { return }
        if (endDate != nil) { return }
        endDate = Date()
    
        let duration = endDate.timeIntervalSince(startDate)
        let expId = self.expId
        
        AnalyticsUtil.faSendMarkerbasedExpDuration(expId: expId, duration: Int(duration))
        L.log(message: "MarkerBased analityc exp=\(expId) duration=\(duration)", event: .info)
        clearDates()
    }
    
    private func clearDates() {
        startDate = nil
        endDate = nil
        expId = -1
    }

// MARK: WTAugmentedRealityViewController parent methods
    
    @objc private func showScanningDialog() {
        let alert = UIAlertController(title: "Notice", message: "Scanning screen will be lost.\nDo you want to leave?", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Leave", style: UIAlertAction.Style.default, handler: { [weak self] action in
            self?.dismissProofing()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc private func endTrial() {
        BLUHelper.instance().endTrial()
    }
    
    @objc private func initializeWorld() {
        customizeWorldInterface()
        architectView.callJavaScript("NativeBridge.init()")
    }
    
    @objc private func customizeWorldInterface() {
        guard let fileUrl = Bundle.main.url(forResource:   "customization",
                                            withExtension: "json",
                                            subdirectory:  "BLUcustomization")
        else {
            L.log(message: "Cannot find customization.json file", event: .info); return
        }

        guard let customizationData = FileManager.default.contents(atPath: fileUrl.path)
        else {
            L.log(message: "Cannot read contents of customization.json file", event: .info); return
        }

        guard let customizationJson = String(data: customizationData, encoding: String.Encoding.utf8)
        else {
                L.log(message: "Cannot encode contents of customization.json file", event: .info); return
        }
        
        let rawJsonString = customizationJson.replacingOccurrences(of: "\n", with: "")
        
        architectView.callJavaScript("NativeBridge.setupUiCustomizationData('\(rawJsonString)')")
    }
    
    @objc private func sendPauseEvent() {
        architectView.callJavaScript("NativeBridge.onPause()")
    }
    
    @objc private func sendResumeEvent() {
        architectView.callJavaScript("NativeBridge.onResume()")
    }
    
    @objc private func modelPlayStart(_ modelId: Int32) {
        if (!playModels.keys.contains(modelId)){
            playModels[modelId] = Date()
             L.log(message: "Markerless analityc modelPlayStart", event: .info)
        }
    }
    
    @objc private func modelPlayStopAll() {
        L.log(message: "Markerless analityc modelPlayStopAll", event: .info)
        
        if (playModels.count) > 0 {
            
            let endDate = Date()
            var result: [Int: Int] = [Int : Int]()
            
            for (e, value) in playModels  {
                let duration = Int(endDate.timeIntervalSince(value))
                result[Int(e)] = duration
                L.log(message: "Markerless analityc id=\(e) duration=\(duration)", event: .info)
            }
            AnalyticsUtil.faSendMarkerlessExpDuration(ExpIdWithDurationList: result)
            
            playModels = [Int32 : Date]()
        }
    }
    
    @objc private func applicationWillResignActive(notification: NSNotification) {
        self.sendMarkerlessDurationAnalytics()
        self.sendPlayAnalitycs()
    }
    
    private func sendMarkerlessDurationAnalytics() {
        let arType1 = SwiftFromObjCBridge.sInst().arType
        let duration = Date().timeIntervalSince(sessionStartTime)
        AnalyticsUtil.faSendMarkerlessSessionDuration(arType: arType1, durationSeconds: Int(duration))
        sessionStartTime = Date()
    }
}
