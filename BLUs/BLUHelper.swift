
import Foundation

@objc class BLUHelper: NSObject, BLULocationManagerDelegate {
    
    private(set) var isInitializingScan = false
    private var augmentedRealityExperiencesManager: WTAugmentedRealityExperiencesManager = WTAugmentedRealityExperiencesManager();
    private var augmentedRealityAuthenticationRequestManager: WTAuthorizationRequestManager = WTAuthorizationRequestManager();
    private var isMock: Bool = false // USE it FOR DEBUG
    private var isApplicationInitialized: Bool = false
    private var emailUserSelf: String = ""
    
    @objc class var inst: BLUHelper {
        struct Singleton {
            static let inst = BLUHelper()
        }
        return Singleton.inst
    }
    
    @objc class func instance() -> BLUHelper {
        return BLUHelper.inst
    }
    
    ////>>>> DEBUG
    func mock() {
        isMock = true
    }
    func unMock() {
        isMock = false
    }
    func isMocking() -> Bool {
        return isMock
    }
    ////<<<<<
    
    
    func initialize(bluLicenseKey: String, onSuccess: @escaping (() -> Void), onError: @escaping ((_ message: String) -> Void)) {
        
        SwiftFromObjCBridge.sInst().bluLicenseKey = bluLicenseKey
        
        BLULocationManager.sInst().searchLocation(locationDefined: nil)
        BLULocationManager.sInst().delegate = self
        
        if !self.augmentedRealityExperiencesManager.hasLoadedAugmentedRealityExperiencesFromPlist() {
            self.augmentedRealityExperiencesManager.loadARExperience({  _ in
                // L.log(message: "Application initialization complete", event: .info)
                
                self.initScanAR(onSuccess: onSuccess) { (m) in
                    onError(m)
                }
                //do nothing
            }, scanCameraPositionBack: true)
        } else {
            initScanAR(onSuccess: onSuccess) { (m) in
                onError(m)
            }
        }
    }
    
    func searchLoaction(didFindLocation loc: BLULocation?) {
        //L.log(message: "LOCATION Lat: \(String(describing: loc?.latitude)), Lon:\(String(describing: loc?.longitude))", event: .info)
    }
    
    func initScanAR(onSuccess: @escaping (() -> Void), onError: @escaping ((_ message: String) -> Void)) {
        if isInitializingScan {
            L.log(message: AConstants.MESSAGE_AR_INIT_IN_PROGRESS, event: .warning)
            onError(AConstants.MESSAGE_AR_INIT_IN_PROGRESS)
            return
        }
        isInitializingScan = true
        
        prepareForARScreen(arIndex: 1)
        
        self.requestARInitData(shouldDefineLocation: true, onSuccess: onSuccess, onError: onError)
    }
    
    func initGeoAR(onSuccess: @escaping (() -> Void)) {
        prepareForARScreen(arIndex: 2)
        
        onSuccess()
    }
    
    func initApplication(onSuccess: @escaping (() -> Void), onError: @escaping ((_ message: String) -> Void)) {
        if isApplicationInitialized {
            onSuccess()
            return
        }
        
        if isInitializingScan {
            L.log(message: AConstants.MESSAGE_AR_INIT_IN_PROGRESS, event: .warning)
            onError(AConstants.MESSAGE_AR_INIT_IN_PROGRESS)
            return
        }
        isInitializingScan = true
        
        self.requestARInitData(shouldDefineLocation: false, onSuccess: onSuccess, onError: onError)
    }
    
    func requestARInitData(emailUserSelf: String = PrefsUtil.instance.getEmailUserSelf(),
                           shouldDefineLocation: Bool, onSuccess: @escaping (() -> Void), onError: @escaping ((_ message: String) -> Void)) {
        
        RESTUtil.instance.requestARInitData(emailUserSelf: emailUserSelf, onSuccess: { (data) in
            L.log(message: "requestARInitData received data: \(data)", event: .info)
            func initARExperience() {
                self.augmentedRealityExperiencesManager.loadARExperience({_  in
                    self.isInitializingScan = false
                    
                    let isLocadionAllowed = BLULocationManager.sInst().areLocationPermissionsAvailable()
                    
                    let lastLoc = PrefsUtil.instance.getLastKnownLocation()
                    if (lastLoc.latitude == 0 && lastLoc.longitude == 0 && isLocadionAllowed) {
                        L.log(message: "LOCATION IS NULL, waiting for obtaining location", event: .info)
                        BLULocationManager.sInst().searchLocation(locationDefined: { (loc) in
                            self.isApplicationInitialized = true
                            onSuccess()
                        })
                    } else {
                        L.log(message: "LOCATION IS \(lastLoc.latitude),\(lastLoc.longitude))", event: .info)
                        BLULocationManager.sInst().searchLocation(locationDefined: nil)
                        self.isApplicationInitialized = true
                        onSuccess()
                    }
                }, scanCameraPositionBack: true)
                
            }
            
            if var minVersion = data.min_sdk_version {
                if self.isMocking() {
                    minVersion = 999
                }
                PrefsUtil.instance.saveMinSDKVersion(version: minVersion)
                if AConstants.CURRENT_BLU_SDK_VERSION < minVersion {
                    _ = RESTUtil.instance.checkSDKVersionFailed()
                    self.isInitializingScan = false
                    onError(AConstants.getOutdatedSDKMessage())
                    return
                }
            } else {
                PrefsUtil.instance.saveMinSDKVersion(version: self.isMocking() ? 999 : 1) // if no version field in response, let users to work with SDK
            }
            
            //check required data items for nil - clientToken and CollectionId. If any of them is nil - use local js
            if let clientToken = data.client_token,
                let collectionId = data.wikitude_collection,
                let proofingCollectionId = data.proofing_wikitude_collection,
                let arKey = data.ar_key {
                L.log(message: "init scan AR success", event: .info)
                
                SwiftFromObjCBridge.sInst().licenseKey = arKey
                
                let groupId = data.group_id ?? ""
                PrefsUtil.instance.saveMarkerbasedClientData(clientToken: clientToken, collectionId: collectionId, groupId: groupId, proofingCollectionId: proofingCollectionId)
                PrefsUtil.instance.saveEmailUserSelf(emailUserSelf: emailUserSelf)
                
                if shouldDefineLocation {
                    initARExperience()
                } else {
                    self.isInitializingScan = false
                    self.isApplicationInitialized = true
                    onSuccess()
                }
            } else {
                L.log(message: "Init data is missing license key, collection id or client token. Unable to continue with AR Expereicne", event: .error)
                self.isInitializingScan = false
                
                if shouldDefineLocation {
                    BLULocationManager.sInst().searchLocation(locationDefined: nil)
                }
                onError("Unable to obtain initialization data. AR license key is missing.")
            }
        }, onError: {(message) in
            
            self.isInitializingScan = false
            if shouldDefineLocation {
                BLULocationManager.sInst().searchLocation(locationDefined: nil)
            }
            onError(message)
        })
    }
    
    func switchEnvironment(environmentType: APIHandler?) {
        clearInitData()
        RESTUtil.instance.updateApi(newApiType: environmentType)
    }
    
    private func clearInitData() {
        SwiftFromObjCBridge.sInst().licenseKey = ""
        PrefsUtil.instance.saveMarkerbasedClientData(clientToken: "", collectionId: "", groupId: "", proofingCollectionId: "")
        self.isApplicationInitialized = false;
    }
    
    func downloadMarkerlessARExperiences(markerlessExperiencesArray: [MarkerlessExperience],
                                         onCancelDownload: @escaping (() -> Void),
                                         onDownloadComplete: @escaping (() -> Void),
                                         onProgress: @escaping ((Float) -> Void)) {
        
        BLULocationManager.sInst().searchLocation(locationDefined: nil)
        
        prepareForARScreen(arIndex: 0)
        
        let onArrayOfPathsChanged = {
            onDownloadComplete()
        }
        SwiftFromObjCBridge.sInst().onPathsArrayChanged = onArrayOfPathsChanged
        
        MarkerlessDownloadUtil.sharedInstance().startDownloadingMarkerlessData(experiencesArray: markerlessExperiencesArray,
                                                                               onCancelDownload: { (cancelMessage) in
                                                                                //all this are called after download is cancelled
                                                                                onCancelDownload()
        }) { (progress) in
            onProgress(progress)
        }
    }
    
    func cancelDownloadingMarkerlessData() {
        MarkerlessDownloadUtil.sharedInstance().cancelDownloading()
    }
    
    func setEmailUserSelf(emailUserSelf: String, onSuccess: @escaping (() -> Void), onError: @escaping ((_ message: String) -> Void)) {
        
        if (!emailUserSelf.isEmpty){
            self.requestARInitData(emailUserSelf: emailUserSelf, shouldDefineLocation: true, onSuccess: onSuccess, onError: onError)
        }else{
            endTrial()
            onSuccess()
        }
    }
    
    func endTrial(){
        PrefsUtil.instance.saveEmailUserSelf(emailUserSelf: emailUserSelf)
        self.clearInitData()
    }
    
    // PRIVATE functions
    
    private func prepareForARScreen(arIndex: Int32) { //0 = instant, 1 = scan, 2 = geo
        SwiftFromObjCBridge.sInst().arExperience = self.augmentedRealityExperiencesManager.augmentedRealityExperience(forPosition: arIndex);
        SwiftFromObjCBridge.sInst().isMarkerlessAR = arIndex == 0
        SwiftFromObjCBridge.sInst().arType = Int(arIndex)
    }
}

extension BLUHelper {
    func prepareMarkerbasedExperience(withOptions options: MarkerbasedOptions, _ completion: @escaping PreparationCallback) {
        
        guard (AVCaptureDevice.authorizationStatus(for: .video) == .authorized) else {
            DispatchQueue.main.async {
                completion(nil, BLUBasicError(.error, description: AConstants.ERROR_MESSAGE_CAMERA_ACCESS))
            }
            return
        }
        
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { hasAccess in
            if !hasAccess {
                DispatchQueue.main.async {
                    completion(nil, BLUBasicError(.error, description: AConstants.ERROR_MESSAGE_CAMERA_ACCESS))
                }
                return
            }
        }
        
        guard (UIDevice.isSimulator == false) else {
            DispatchQueue.main.async {
                completion(nil, BLUBasicError(.error, description: AConstants.ERROR_MESSAGE_SIMULATOR_USAGE))
            }
            return
        }
        
        if isInitializingScan {
            L.log(message: AConstants.MESSAGE_AR_INIT_IN_PROGRESS, event: .warning)
            completion(nil, BLUBasicError(.warning, description: AConstants.MESSAGE_AR_INIT_IN_PROGRESS))
        }
        isInitializingScan = true
        
        prepareForARScreen(arIndex: 1)
        
        // MARK: Replace this bifucration of flow with single completion lambda
        let errorBlock: ((_ message: String) -> Void) = { message in
            DispatchQueue.main.async {
                completion(nil, BLUBasicError(.error, description: message))
            }
        }
        let successBlock: (() -> Void) = {
            DispatchQueue.main.async {
                let arController: BLUARController = options.proofingEnabled ?
                    QRCodeViewController.instantiate() :
                    ARBaseViewController.instantiate()
                arController.options = options
                completion(arController, nil)
            }
        }
        self.requestARInitData(shouldDefineLocation: true, onSuccess: successBlock, onError: errorBlock)
    }
    
    func prepareMarkerlessExperiences(_ experiences: [MarkerlessExperience],
                                        completion: @escaping PreparationCallback,
                                        progress: ((Float) -> Void)? = nil) {
        
        guard (AVCaptureDevice.authorizationStatus(for: .video) == .authorized) else {
            DispatchQueue.main.async {
                completion(nil, BLUBasicError(.error, description: AConstants.ERROR_MESSAGE_CAMERA_ACCESS))
            }
            return
        }
        
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { hasAccess in
            if !hasAccess {
                DispatchQueue.main.async {
                    completion(nil, BLUBasicError(.error, description: AConstants.ERROR_MESSAGE_CAMERA_ACCESS))
                }
            }
        }
        
        guard (UIDevice.isSimulator == false) else {
            DispatchQueue.main.async {
                completion(nil, BLUBasicError(.error, description: AConstants.ERROR_MESSAGE_SIMULATOR_USAGE))
            }
            return
        }
        
        BLULocationManager.sInst().searchLocation(locationDefined: nil)
        
        prepareForARScreen(arIndex: 0)
        
        let onArrayOfPathsChanged = {
            DispatchQueue.main.async {
                completion(ARBaseViewController.instantiate(), nil)
            }
        }
        SwiftFromObjCBridge.sInst().onPathsArrayChanged = onArrayOfPathsChanged
        
        MarkerlessDownloadUtil.sharedInstance()
            .startDownloadingMarkerlessData(experiencesArray: experiences,
                                            onCancelDownload: { (cancelMessage) in
                                                DispatchQueue.main.async {
                                                    completion(nil, BLUBasicError(.error, description: cancelMessage ?? AConstants.ERROR_MESSAGE_DOWNLOADING_USER_CANCEL))
                                                }
            }) { (p) in
                progress?(p)
        }
    }
}
