import Foundation

// Enum for showing the type of Log Types
enum LogEvent: String {
    case error = "‼️" // error
    case info = "ℹ️" // info
    case warning = "⚠️" // warning
    case debug = "🐞" // debug
}


@objc public class L: NSObject {
    static var isEnabled = false;
    static var dateFormat = "hh:mm:ssSSS"
    static var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        formatter.locale = Locale.current
        formatter.timeZone = TimeZone.current
        return formatter
    }
    
    @objc public class func error(message: String) {
        L.log(message: message, event: .error)
    }
    
    @objc public class func info(message: String) {
        L.log(message: message, event: .info)
    }
    
    @objc public class func warning(message: String) {
        L.log(message: message, event: .warning)
    }
    
    @objc public class func debug(message: String) {
        L.log(message: message, event: .debug)
    }
    
    class func log(message: String,
                   event: LogEvent,
                   fileName: String = #file,
                   line: Int = #line,
                   column: Int = #column,
                   funcName: String = #function) {
        
        #if DEBUG
//        if (event == .debug) {
//            print("\(Date().toString()) [BLU \(event.rawValue)] -> \(message)")
//        }
        if (isEnabled || event == .error) {
            print("\(Date().toString()) [BLUairspace \(event.rawValue)] -> \(message)")
        }
        #endif
    }
    
    class func enableLogging(enable: Bool) {
        isEnabled = enable
    }
    
    class func isLoggingEnabled() -> Bool {
        return isEnabled
    }
    
    private class func sourceFileName(filePath: String) -> String {
        let components = filePath.components(separatedBy: "/")
        return components.isEmpty ? "" : components.last!
    }
}

internal extension Date {
    func toString() -> String {
        return L.dateFormatter.string(from: self as Date)
    }
}
