
import Foundation

public protocol APIHandler {
    mutating func endUserLoginEndPoint() -> String
    mutating func endUserEmailPass() -> String
    mutating func getListMarkerlessExperienceEndPoint() -> String
    mutating func getGroupsMarkerlessExperienceEndPoint() -> String
    mutating func getMarkerlessExperienceById() -> String
    mutating func getMarkerbasedTargets() -> String
    mutating func sendFeedBackEndPoint() -> String
    mutating func sendUserInfoEndPoint() -> String
    mutating func getUserPersonalVideo() -> String
    mutating func initAREndPoint() -> String
    mutating func getBluAirSpace() -> String
    mutating func getMarkerlessMarkerbasedUrl() -> String
}

//PROD
class ProdAPIHandler : APIHandler {
    
    public static let ENV_NAME = "PROD"
    
    let commonUrl = "https://api-internal.realityblu.com/prod"
    
    func getMarkerlessExperienceById() -> String {
        return "\(commonUrl)-mobile-experiences/getMarkerlessExperienceById/"
    }

    func initAREndPoint() -> String {
        return "\(commonUrl)-mobile-init/initMobileApp/"
    }
    
    func endUserLoginEndPoint() -> String {
        return "\(commonUrl)-mobile-end-users/endUserLogin"
    }
    
    func endUserEmailPass() -> String {
        return "\(commonUrl)-mobile-end-users/endUserEmailPass"
    }
    
    func getListMarkerlessExperienceEndPoint() -> String {
        return "\(commonUrl)-mobile-experiences/getListMarkerlessExperience/"
    }
    
    func getGroupsMarkerlessExperienceEndPoint() -> String {
        return "\(commonUrl)-mobile-experiences/getGroupsMarkerlessExperience/"
    }
    
    func getMarkerbasedTargets() -> String {
        return "\(commonUrl)-mobile-experiences/getMarkerbasedTargets/"
    }
    
    func sendFeedBackEndPoint() -> String {
        return "\(commonUrl)-mobile-support/sendFeedback"
    }
    
    func sendUserInfoEndPoint() -> String {
        return "\(commonUrl)-pv/createPV"
    }
    
    func getUserPersonalVideo() -> String {
        return "\(commonUrl)-mobile-experiences/getPersonVideoForMobile"
    }
    
    func getBluAirSpace() -> String {
        return "https://bluairspace.realityblu.com"
    }
    
    func getMarkerlessMarkerbasedUrl() -> String {
        return "\(commonUrl)-mobile-experiences/"
    }
}
