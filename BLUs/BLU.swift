import Foundation
import FirebaseCore

public typealias PreparationCallback = (BLUARController?, BLUError?) -> Void
public typealias TaskCallback = (BLUError?) -> Void

public class BLU: NSObject {
    /**
     Аn initialization procedure to make basic SDK preparations for AR experiences.
     - parameter licenseKey: RealityBLU license key, registered for your bundle id
     - parameter completion: Closure to handle initialization completion and errors
     */
    public static func initialize(_ licenseKey: String, completion: TaskCallback? = nil) {
        BLUHelper.instance().initialize(bluLicenseKey: licenseKey, onSuccess: {
            configureAnalytics()
            PrefsUtil.instance.setInstallSettings()
            print("BLUairspaceSDK initialization successful")
            completion?(nil)
        }) { (message) in
            print("BLUairspaceSDK initialization error: \(message)")
            completion?(BLUBasicError(.error, description: message))
        }
    }
    
    /**
     Create Markerbased AR scanning controller and prepare it for launch
     - parameter completion: Closure to handle Markerbased AR preparation procedure result
     */
    public static func prepareMarkerbased(_ completion: @escaping PreparationCallback) {
        SwiftFromObjCBridge.sInst().fakeLocations = nil
        BLUHelper.instance().prepareMarkerbasedExperience(withOptions: MarkerbasedOptions(), completion)
    }
    
    /**
    Create scanning controller using MarkerbasedOptions and prepare it for launch
    - parameter options: `MarkerbasedOptions` object to configure AR scanning controller
    - parameter completion: Closure to handle Markerbased AR preparation procedure result
    */
    public static func prepareMarkerbased(withOptions options: MarkerbasedOptions, completion: @escaping PreparationCallback) {
        SwiftFromObjCBridge.sInst().fakeLocations = nil
        BLUHelper.instance().prepareMarkerbasedExperience(withOptions: options, completion)
    }
    
    /**
     Starts downloading Markerless AR experiences and prepare Markerless AR ViewController for starting when download completes. BLU sdk can support up to 5 different markerless experiences at a time.
     - parameter experiences: Array of markerless experiences. Max length is 5
     - parameter progress: Closure for handling download progress event (e.g show progress dialog)
     - parameter completion: Closure to handle Markerbased AR preparation procedure result
     */
    public static func prepareMarkerlessExperiences(_ experiences: [MarkerlessExperience],
                                                    withProgress progress: ((Float) -> Void)?,
                                                    completion: @escaping PreparationCallback) {
        SwiftFromObjCBridge.sInst().fakeLocations = nil
        
        guard experiences.count < 5 else {
            DispatchQueue.main.async {
                completion(nil, BLUBasicError(.error, description: AConstants.ERROR_MESSAGE_DOWNLOADING_AMOUNT))
            }
            return
        }
        
        BLUHelper.instance().initApplication(onSuccess: {
            BLUHelper.instance().prepareMarkerlessExperiences(experiences, completion: completion, progress: progress)
        }) { (message) in
            L.log(message: "Error: \(message)", event: .warning)
            DispatchQueue.main.async {
                completion(nil, BLUBasicError(.error, description: message))
            }
        }
    }
    /**
     Starts downloading Markerless AR experience with identifier and prepare BLUController for starting when download completes.
     - parameter identifier: Experience to download identifier
     - parameter progress: Closure for handling download progress event (e.g show progress dialog)
     - parameter completion: Closure to handle Markerbased AR preparation procedure result
     */
    public static func prepareMarkerlessExperience(withId identifier: Int,
                                                   andProgress progress: ((Float) -> Void)?,
                                                   completion: @escaping PreparationCallback) {
        BLUHelper.instance().initApplication(onSuccess: {
            RESTUtil.instance.getMarkerlessById(experienceId: identifier, onSuccess: { (exp) in
                BLU.downloadMarkerlessARExperiences(markerlessExperiencesArray: [exp], onCancelDownload: { (cancelMessage) in
                    //do nothing
                }, onDownloadComplete: {
                    DispatchQueue.main.async {
                        completion(ARBaseViewController.instantiate(), nil)
                    }
                }, onProgress: { (p) in
                    progress?(p)
                })
            }, onError: { (message) in
                DispatchQueue.main.async {
                    completion(nil, BLUBasicError(.error, description: message))
                }
            })
        }) { (message) in
            DispatchQueue.main.async {
                completion(nil, BLUBasicError(.error, description: message))
            }
        }
    }

    /**
     Start downloading Markerless experiences and prepares BLUController to appear after download complete. BLU sdk can support up to 5 different markerless experiences at a time.
     - parameter experiences: Array of markerless experiences. Max length is 5
     - parameter completion: Closure to handle Markerbased AR preparation procedure result
     */
    public static func prepareMarkerlessExperiences(_ experiences: [MarkerlessExperience],
                                                    completion: @escaping PreparationCallback) {
        prepareMarkerlessExperiences(experiences, withProgress: nil, completion: completion)
    }
    
    /**
    Start downloading Markerless experience with identifier and prepares BLUController to appear after download complete.
    - parameter identifier: Experience to download identifier
    - parameter completion: Closure to handle Markerbased AR preparation procedure result
    */
    public static func prepareMarkerlessExperience(withId identifier: Int,
                                                   completion: @escaping PreparationCallback) {
        prepareMarkerlessExperience(withId: identifier, andProgress: nil, completion: completion)
    }
}

public class BLUDataHelper {
    /**
     Requests the list of markerbased markers
     - parameter completion: Closure containing markers list
     */
    public static func getMarkerbasedMarkers(completion: @escaping (_ resultData: [MarkerbasedMarker]) -> Void) {
        BLUHelper.instance().initApplication(onSuccess: {
            RESTUtil.instance.getMarkerbasedTargets(resultClosure: completion)
        }) { (message) in
            completion([MarkerbasedMarker]())
        }
    }
    
    /**
     Requests the list of markerless groups
     - parameter completion: Closure for handling markerless groups
     */
    public static func getMarkerlessGroups(_ completion: @escaping (_ resultData: [MarkerlessGroup]) -> Void) {
        BLUHelper.instance().initApplication(onSuccess: {
            RESTUtil.instance.getListMarkerlessExperience(resultClosure: completion)
        }) { (message) in
            completion([MarkerlessGroup]())
        }
    }
    
    /**
     Requests the list of markerless experiences by group
     - parameter groupId: Selected markerless group identifier
     - parameter completion: Closure for handling markerless experiences list response
     */
    public static func getMarkerlessExperiences(groupId: Int, completion: @escaping (_ resultData: [MarkerlessExperience]) -> Void) {
        BLUHelper.instance().initApplication(onSuccess: {

            RESTUtil.instance.getGroupsMarkerlessExperience(listId: groupId) { (subgroups) in
                if let anyAccountId = subgroups.randomElement()?.account_id {
                    MarkerlessDownloadUtil.sharedInstance().accountId = anyAccountId
                }
                
                var experiences = [MarkerlessExperience]()
                for group in subgroups {
                    if let exps = group.experiences {
                        var fullExps = [MarkerlessExperience]()
                        
                        var i = 0
                        for e in exps {
                            fullExps.append(e)
                            //  set group and subgroup names to each experience object
                            fullExps[i].group = group.list_name
                            fullExps[i].subgroup = group.group_name
                            i += 1
                        }
                        experiences.append(contentsOf: fullExps)
                    }
                }
                completion(experiences)
            }
        }) { (message) in
            completion([MarkerlessExperience]())
        }
    }
}

public class BLUUserHelper {
    public static func sendFeedback(_ feedback: FeedbackData, completion: @escaping TaskCallback) {
        BLUHelper.instance().initApplication(onSuccess: {
            RESTUtil.instance.sendFeedback(name: feedback.name, email: feedback.email, phone: feedback.phone, comment: feedback.comment, os: feedback.os, appId: feedback.appId, captcha: feedback.captcha,
                                           onSuccess: { completion(nil) },
                                           onError: { completion(BLUBasicError(.error, description: "unhandled REST Service Error"))})
        }) { (message) in
            completion(BLUBasicError(.error, description: message))
        }
    }
    public static func sendUserInfo(info: UserInfo,
                                    onSuccess: @escaping (() -> Void),
                                    onError: @escaping (() -> Void)) {
        BLUHelper.instance().initApplication(onSuccess: {
            RESTUtil.instance.saveUserInfo(info: info, onSuccess: onSuccess, onError: onError)
        }) { (message) in
            onError()
        }
    }
}

public class BLUTrialHelper {
    public static func getTrialEmail() -> String {
        return PrefsUtil.instance.getEmailUserSelf()
    }
    public static func setTrial(email trialEmail: String, onSuccess: @escaping (() -> Void), onError: @escaping ((_ message: String) -> Void)) {
        BLUHelper.instance().setEmailUserSelf(emailUserSelf: trialEmail, onSuccess: onSuccess, onError: onError)
    }
}

internal extension BLU {
    
    static func initScanAR(onSuccess: @escaping (() -> Void), onError: @escaping ((_ message: String) -> Void)) {
        SwiftFromObjCBridge.sInst().fakeLocations = nil
        
        BLUHelper.instance().initScanAR(onSuccess: {
            onSuccess()
        }) { (message) in
            onError(message)
        }
    }
    
    static func downloadMarkerlessARExperiences(markerlessExperiencesArray: [MarkerlessExperience],
                                                       onCancelDownload: @escaping ((String) -> Void),
                                                       onDownloadComplete: @escaping (() -> Void),
                                                       onProgress: @escaping ((Float) -> Void)) {
        SwiftFromObjCBridge.sInst().fakeLocations = nil
        
        if markerlessExperiencesArray.count > 5 {
            onCancelDownload("Invalid experiences array count")
            return
        }
        
        BLUHelper.instance().initApplication(onSuccess: {
            BLUHelper.instance().downloadMarkerlessARExperiences(markerlessExperiencesArray: markerlessExperiencesArray,
                                                                 onCancelDownload: {
                                                                    onCancelDownload("")
            }, onDownloadComplete: {
                onDownloadComplete()
            }) { (progress) in
                onProgress(progress)
            }
        }) { (message) in
            L.log(message: "Error: \(message)", event: .warning)
            onCancelDownload(message)
        }
    }
    
    static func getMarkerlessById(experienceId: Int, onDownloadComplete: @escaping (() -> Void),
                                         onDownloadProgress: @escaping ((Float) -> Void),
                                         onError: @escaping ((String) -> Void)) {
        BLUHelper.instance().initApplication(onSuccess: {
            RESTUtil.instance.getMarkerlessById(experienceId: experienceId, onSuccess: { (exp) in
                BLU.downloadMarkerlessARExperiences(markerlessExperiencesArray: [exp], onCancelDownload: { (cancelMessage) in
                    //do nothing
                }, onDownloadComplete: {
                    onDownloadComplete()
                }, onProgress: { (progress) in
                    onDownloadProgress(progress)
                })
            }, onError: { (message) in
                onError(message)
            })
        }) { (message) in
            onError(message)
        }
    }
 
    static func getMarkerlessGroups(listId: Int, accountId: Int, completion: @escaping (_ resultData: [MarkerlessExperienceGroup]) -> Void) {
        BLUHelper.instance().initApplication(onSuccess: {
            MarkerlessDownloadUtil.sharedInstance().accountId = accountId
            RESTUtil.instance.getGroupsMarkerlessExperience(listId: listId, resultClosure: completion)
        }) { (message) in
            completion([MarkerlessExperienceGroup]())
        }
    }

    static func cancelDownloadingMarkerlessData() {
        BLUHelper.instance().cancelDownloadingMarkerlessData()
    }
    
    static func isSDKOutdated() -> Bool {
        return !PrefsUtil.instance.isCurrentSDKVersionValid()
    }
    
    static func configureAnalytics() {
        if FirebaseApp.app() == nil {
            FirebaseApp.configure()
            DebugTools.enableAnalytics(enable: true)
            
            if (!PrefsUtil.instance.isInstallSettings()){
                AnalyticsUtil.setEnvironment(EnvironmentName: ProdAPIHandler.ENV_NAME)
            }
        }
    }
    
    static func applicationDidFinishLaunchingWithOptions() { }
    
    static func login(userName: String, password: String,  onResult: @escaping (_ success: Bool) -> Void) {
        BLUHelper.instance().initApplication(onSuccess: {
            RESTUtil.instance.login(userName: userName, password: password, onResult: onResult)
        }) { (message) in
            onResult(false)
        }
    }
    
    static func resetPassword(email: String, onResult: @escaping (_ success: Bool) -> Void) {
        BLUHelper.instance().initApplication(onSuccess: {
            RESTUtil.instance.resetPassword(email: email, onResult: onResult)
        }) { (message) in
            onResult(false)
        }
    }
}
