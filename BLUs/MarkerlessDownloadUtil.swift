import Foundation


@objc class MarkerlessDownloadUtil: NSObject {
    
    @objc class var swiftSharedInstance: MarkerlessDownloadUtil {
        struct Singleton {
            static let instance = MarkerlessDownloadUtil()
        }
        return Singleton.instance
    }
    
    // the sharedInstance class method can be reached from ObjC
    @objc class func sharedInstance() -> MarkerlessDownloadUtil {
        return MarkerlessDownloadUtil.swiftSharedInstance
    }
    //<<<<<
    
    var isDownloadingExperiences: Bool = false;
    var downloadIncrement = 0
    var cancelled = false;
    var filesDownloaded: Int = 0
    var accountId: Int = 1
    var filesCached: Int = 0
    
    var idsArray = [Int]()
    var animationsArray = [String]()
    var audioArray = [MExperienceAudio]()
    var scaleMobileArray = [Float]()
    var transformsArray = [Transform]()
    var urlsArray = [String]()
    var localFileUrlsArray = [String]()
    
    var onDownloadCancel: ((String?) -> Void)? //method to be called in some viewcontroller
    var onProgressUpdated: ((Float) -> Void)? //method to be called in some viewcontroller

    func startDownloadingMarkerlessData(experiencesArray: [MarkerlessExperience], onCancelDownload: @escaping ((String?) -> Void), onProgress: @escaping ((Float) -> Void)) {
        L.log(message: "startDownloadingData, count of files to download: \(experiencesArray.count)", event: .info)
        
        if isDownloadingExperiences {
            L.log(message: "download is in progress, aborting", event: .warning)
            return
        }
        
        idsArray.removeAll()
        animationsArray.removeAll()
        audioArray.removeAll()
        scaleMobileArray.removeAll()
        transformsArray.removeAll()
        urlsArray.removeAll()
        localFileUrlsArray.removeAll()
        downloadIncrement = 0
        filesDownloaded = 0
        cancelled = false;
        filesCached = 0
        
        if experiencesArray.isEmpty {
            L.log(message: "Error downloading markerless data, urls array is empty", event: .error)
            onCancelDownload("Error downloading markerless data. Experiences collection is empty")
            return
        }
        
        for e in experiencesArray {
            if let id = e.id {
                idsArray.append(id)
            } else {
                idsArray.append(0)
            }
            if let animation = e.obj_animations {
                animationsArray.append(animation)
            } else {
                animationsArray.append("")
            }
           
            audioArray.append(MExperienceAudio(audio: e.audio_file, loop: e.audio_loop))
            
            if let scaleMobile = e.scale_mobile {
                scaleMobileArray.append(scaleMobile)
            } else {
                scaleMobileArray.append(0.01)
            }
            if let rot = e.rotation, let pos = e.position, let scale = e.scale {
                transformsArray.append(Transform(fromComponents: Point3D(fromArray: scale), pos: Point3D(fromArray: pos), rot: Point3D(fromArray: rot)))
            } else {
                transformsArray.append(Transform()) //add default transform
            }
            if let fileName = e.file_name {
                addUrlToArray(url: fileName)
            } else {
                urlsArray.append(AConstants.WT3_PLACEHOLDER_AR)
            }
            if let icon = e.icon {
                addUrlToArray(url: icon)
            } else {
                urlsArray.append(AConstants.IMG_PLACEHOLDER_AR)
            }
        }
        
        AnalyticsUtil.faSendMarkerlessModelsUsed(experienceIdsArray: idsArray) //called after "view in room" clicked. No matter if models cached or not, this will show what models users use most
        
        onDownloadCancel = onCancelDownload
        onProgressUpdated = onProgress
        
        downloadDataInSequentially()
    }
    
    private func addUrlToArray(url: String) {
        if !url.isEmpty {
            urlsArray.append(url)
        } else {
            L.log(message: "got empty URL in urls array, ignoring it", event: .warning)
        }
    }
    
    func removeJSUndigestCharacters(value: String) -> String {
        //wikitude js file don't like many unicode characters. We just use their unicode ids without % sign for local names and replace + signs
        return value.replacingOccurrences(of: "%", with: "")
            .replacingOccurrences(of: "\"", with: "")
            .replacingOccurrences(of: "\'", with: "")
            .replacingOccurrences(of: "+", with: "_")
    }
    
    func downloadDataInSequentially() {
        isDownloadingExperiences = true;
        let urlStringOriginal = urlsArray[0];
        
        if urlStringOriginal == AConstants.WT3_PLACEHOLDER_AR {
            L.log(message: "3D model file is missing, using placeholder", event: .warning)
            self.handleDownloadedData(forParh: WikitudeUtil.sharedInstance().getWT3PlaceholderAssetPath())
            return
        }
        
        if urlStringOriginal == AConstants.IMG_PLACEHOLDER_AR {
            L.log(message: "Icon file is missing, using placeholder", event: .warning)
            self.handleDownloadedData(forParh: WikitudeUtil.sharedInstance().getIconPlaceholderAssetPath())
            return
        }
        
        let urlString = urlStringOriginal.replacingOccurrences(of: ".fbx", with: ".wt3", options: .caseInsensitive, range: nil)
        let fileNameOriginal = urlString.components(separatedBy: "/").last!
        
        let fileName = removeJSUndigestCharacters(value: fileNameOriginal)
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsURL:NSURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
            let fileURL = documentsURL.appendingPathComponent("\(fileName)")
            return (fileURL!,[.removePreviousFile, .createIntermediateDirectories])
        }
        
        //////////try get already downloaded models if they exist
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = URL(fileURLWithPath: path)
        
        let existingFilePath = url.appendingPathComponent("\(fileName)").path
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: existingFilePath) {
            L.log(message: "file \(existingFilePath) is already cached", event: .info)
            self.filesCached += 1
            self.handleDownloadedData(forParh: existingFilePath)
        } else {
            L.log(message: "file \(existingFilePath) created, downloading...", event: .info)
            SessionManager.default.download(urlString, to: destination).downloadProgress(closure: { (prog) in
                self.onProgressUpdated?(Float(prog.fractionCompleted))
            }).response { response in
                self.filesDownloaded += 1
                
                if response.error == nil, let filePath = response.destinationURL?.path {
                    self.handleDownloadedData(forParh: filePath)
                } else {
                    L.log(message: "error downloading file \(existingFilePath), \(response.error.debugDescription)", event: .warning)
                    self.handleDownloadedData(forParh: "")
                }
            }
        }
    }
    
    func handleDownloadedData(forParh filePath: String) {
        self.urlsArray.removeFirst()
        
        self.localFileUrlsArray.append(filePath)
        
        if !self.urlsArray.isEmpty{
            self.downloadIncrement += 1;
            
            self.downloadDataInSequentially()
        } else {
            L.log(message: "Markerless data downloading has finished, files downloaded = \(self.downloadIncrement - self.filesCached + 1)", event: .info)
            self.downloadIncrement = 0;
            self.isDownloadingExperiences = false
            
            if (self.filesDownloaded > 0) {
                AnalyticsUtil.faSendMarkerlessClick(accountId: accountId)
                self.filesDownloaded = 0
                self.createExperiencePathsJSON()
            } else {
                self.createExperiencePathsJSON()
            }
        }
    }
    
    func createExperiencePathsJSON() {
        if !cancelled && !self.localFileUrlsArray.isEmpty {
            var pathsDict = [[String:Any]]()
            
            var i = 0
            var k = 0
            while i < self.localFileUrlsArray.count {
                let id = self.idsArray[k]
                let filePath = self.localFileUrlsArray[i]
                let iconBase64 = URL(fileURLWithPath: self.localFileUrlsArray[i + 1]).base64string()
                let iconPath = "data:image/png;base64, \(iconBase64)"
                let animation = self.animationsArray[k]
                let audio = self.audioArray[k]
                let scaleMob = self.scaleMobileArray[k]
                let transform = self.transformsArray[k]
                
                let item: [String: Any] = [
                    "id": id,
                    "filePath": filePath,
                    "iconPath": iconPath,
                    "obj_animations": animation,
                    "scale_mobile": scaleMob,
                    "audioFile": audio.audioFile,
                    "audioLoop": audio.audioLoop,
                    "modelScale": [transform.scale.x, transform.scale.y, transform.scale.z],
                    "modelPosition": [transform.position.x, transform.position.y, transform.position.z],
                    "modelRotation": [transform.rotation.x, transform.rotation.y, transform.rotation.z],
                ]
                
                pathsDict.append(item)
                
                k = k + 1
                i = i + 2
            }
            
            let pathsArrayJson = JSON(pathsDict)
            
            //convert the JSON to a raw String
            if let pathsArrayString = pathsArrayJson.rawString(options: []) {
                SwiftFromObjCBridge.sInst().experiencesPathsArrayString = pathsArrayString
            }
        }
    }
    
    func cancelDownloading() {
//        let sessionManager = Alamofire.SessionManager.default;
//        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
//            dataTasks.forEach { $0.cancel() }
//            uploadTasks.forEach { $0.cancel() }
//            downloadTasks.forEach { $0.cancel() }
//
//            self.cancelled = true;
//            self.isDownloadingExperiences = false
//
//            DispatchQueue.main.async(execute: {() -> Void in
//                self.onDownloadCancel?()
//            })
//        }
    }
}
