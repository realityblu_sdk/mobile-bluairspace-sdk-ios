

import Foundation
import SystemConfiguration
import UIKit
import AdSupport

class ATools {
    
    static let instance = ATools()
    private init() {
    }
    
    func getTimeStamp() -> String {
        let stringFromDate = Date().iso8601    // "2017-03-22T13:22:13.933Z"
        
        if let dateFromString = stringFromDate.dateFromISO8601 {
            return dateFromString.iso8601;
        }
        return "";
    }
    
    func parseDateIso8601(dateString: String?) -> Date {
        if let result = dateString?.dateFromOtherISO8601 {
            return result
        }
        return Date();
    }
    
    func getAppVersionString() -> String {
        let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String // e.g 2.0
        let build = Bundle.main.infoDictionary!["CFBundleVersion"] as! String // e.g 4
        let result = "\(version).\(build)"
        //L.log(message: "App version is \(result)", event: .info)
        
        return result
    }
    
    func getOSVersion() -> String {
        return UIDevice.current.systemVersion
    }
    
    // Adveritising ID - IDFA
    func getDeviceId() -> String {
        return ASIdentifierManager.shared().advertisingIdentifier.uuidString
    }
}
