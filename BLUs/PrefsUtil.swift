

import Foundation

class PrefsUtil {
    
    static let instance = PrefsUtil()
    
    
    func setInstallSettings(){
        let preferences = UserDefaults.standard
        preferences.set(true, forKey: AConstants.PREFS_KEY_INSTALL_SETTINGS)
        didSave(preferences: preferences)
    }
    
    func isInstallSettings() -> Bool {
        let preferences = UserDefaults.standard
        return preferences.bool(forKey: AConstants.PREFS_KEY_INSTALL_SETTINGS)
    }
    
    func saveUserPersonalInfo(userInfo: UserInfo){
        let preferences = UserDefaults.standard
        preferences.set(userInfo.firstName, forKey: AConstants.PREFS_KEY_USER_FIRST_NAME)
        preferences.set(userInfo.secondName, forKey: AConstants.PREFS_KEY_USER_FAMILY_NAME)
        preferences.set(userInfo.email, forKey: AConstants.PREFS_KEY_USER_EMAIL)
        preferences.set(userInfo.businessName, forKey: AConstants.PREFS_KEY_USER_BUSINESS_NAME)
        preferences.set(userInfo.industry, forKey: AConstants.PREFS_KEY_USER_INDUSTRY)
        preferences.set(userInfo.music, forKey: AConstants.PREFS_KEY_USER_MUSIC)
        preferences.set(userInfo.gender, forKey: AConstants.PREFS_KEY_USER_GENDER)
        didSave(preferences: preferences)
    }
    
    func getUserInfo() -> UserInfo {
        let preferences = UserDefaults.standard
        let fname = preferences.string(forKey: AConstants.PREFS_KEY_USER_FIRST_NAME)
        let sname = preferences.string(forKey: AConstants.PREFS_KEY_USER_FAMILY_NAME)
        let bname = preferences.string(forKey: AConstants.PREFS_KEY_USER_BUSINESS_NAME)
        let email = preferences.string(forKey: AConstants.PREFS_KEY_USER_EMAIL)
        let music = preferences.string(forKey: AConstants.PREFS_KEY_USER_MUSIC)
        let gender = preferences.string(forKey: AConstants.PREFS_KEY_USER_GENDER)
        let industry = preferences.string(forKey: AConstants.PREFS_KEY_USER_INDUSTRY)
        
        guard fname != nil, sname != nil, bname != nil, email != nil, music != nil, gender != nil, industry != nil else {
            return UserInfo()
        }
        return UserInfo(firstName: fname!, secondName: sname!,
                        email: email!, businessName: bname!,
                        industry: industry!, music: music!,
                        gender: gender!)
    }
    
    
    func activateMarkerLessExperienceDoNotShowTips() {
        let preferences = UserDefaults.standard
        preferences.set(true, forKey: AConstants.PREFS_KEY_MARKERLESS_DO_NOT_SHOW_TIPS)
        didSave(preferences: preferences)
    }
    
    func getMarkerLessExperienceDoNotShowTipsActivated() -> Bool {
        let preferences = UserDefaults.standard
        let notShowActivated = preferences.bool(forKey: AConstants.PREFS_KEY_MARKERLESS_DO_NOT_SHOW_TIPS)
        
        return notShowActivated
    }
    
    func saveMinSDKVersion(version: Int) {
        let preferences = UserDefaults.standard
        preferences.set(version, forKey: AConstants.PREFS_KEY_CURRENT_BLU_SDK_VERSION)
        didSave(preferences: preferences)
    }
    
    func getMinSDKInitVersion() -> Int {
        let preferences = UserDefaults.standard
        let version = preferences.integer(forKey: AConstants.PREFS_KEY_CURRENT_BLU_SDK_VERSION)
        
        return version
    }
    
    func isCurrentSDKVersionValid() -> Bool {
        return AConstants.CURRENT_BLU_SDK_VERSION >= getMinSDKInitVersion()
    }
    
    func saveLastKnownLocation(loc: BLULocation) {
        let preferences = UserDefaults.standard
        preferences.set(loc.latitude, forKey: AConstants.PREFS_KEY_LAST_LOCATION_LT)
        preferences.set(loc.longitude, forKey: AConstants.PREFS_KEY_LAST_LOCATION_LN)
        didSave(preferences: preferences)
    }
    
    func getLastKnownLocation() -> BLULocation {
        let preferences = UserDefaults.standard
        return BLULocation(latitude: preferences.double(forKey: AConstants.PREFS_KEY_LAST_LOCATION_LT),
                           longitude: preferences.double(forKey: AConstants.PREFS_KEY_LAST_LOCATION_LN),
                           address: nil)
    }
    
    func saveMarkerbasedClientData(clientToken: String, collectionId: String, groupId: String, proofingCollectionId: String) {
        let preferences = UserDefaults.standard
        L.log(message: "scan init data: clientToken: \(clientToken), collectionId: \(collectionId), group: \(groupId), proofingCollectionId: \(proofingCollectionId)", event: .info)
        preferences.set(clientToken, forKey: AConstants.PREFS_KEY_MARKERBASED_CLIENT_TOKEN)
        preferences.set(collectionId, forKey: AConstants.PREFS_KEY_MARKERBASED_COLLECTION_ID)
        preferences.set(groupId, forKey: AConstants.PREFS_KEY_MARKERBASED_GROUP_ID)
        preferences.set(proofingCollectionId, forKey: AConstants.PREFS_KEY_MARKERBASED_PROOFING_COLLECTION_ID)
        didSave(preferences: preferences)
    }
    
    func getMarkerbasedClientData() -> (String, String, String, String) {
        let preferences = UserDefaults.standard
        if let clientToken = preferences.string(forKey: AConstants.PREFS_KEY_MARKERBASED_CLIENT_TOKEN),
            let collectionId = preferences.string(forKey: AConstants.PREFS_KEY_MARKERBASED_COLLECTION_ID),
            let groupId = preferences.string(forKey: AConstants.PREFS_KEY_MARKERBASED_GROUP_ID),
            let proofingCollectionId = preferences.string(forKey: AConstants.PREFS_KEY_MARKERBASED_PROOFING_COLLECTION_ID)        {
            return (clientToken, collectionId, groupId, proofingCollectionId)
        }
        
        return ("", "", "", "")
    }
    func saveEmailUserSelf(emailUserSelf: String) {
        let preferences = UserDefaults.standard
        L.log(message: "save user self: userEmailSelf: \(emailUserSelf)", event: .info)
        preferences.set(emailUserSelf, forKey: AConstants.PREFS_KEY_USER_EMAIL_SELF)
        didSave(preferences: preferences)
    }
    
    func getEmailUserSelf() -> (String) {
        let preferences = UserDefaults.standard
        if let userEmailSelf = preferences.string(forKey: AConstants.PREFS_KEY_USER_EMAIL_SELF){
            return (userEmailSelf)
        }
        return ("")
    }
    
    
    func setNeedMarkerBasedTutorial(value: Bool) {
        let preferences = UserDefaults.standard
        preferences.set(value, forKey: AConstants.PREFS_KEY_IS_NEED_MARKERBASED_TUTORIAL)
        didSave(preferences: preferences)
    }
    
    func getIsNeedMarkerBasedTutorial() -> (Bool) {
        let preferences = UserDefaults.standard
        preferences.register(defaults: [AConstants.PREFS_KEY_IS_NEED_MARKERBASED_TUTORIAL : true])
        let value = preferences.bool(forKey: AConstants.PREFS_KEY_IS_NEED_MARKERBASED_TUTORIAL)
        return value
    }
    
    //getIsNeedMarkerBasedTutorial
    
    
//    func saveAnlyticsPrefs(enable: Bool) {
//        let preferences = UserDefaults.standard
//        preferences.set(enable, forKey: AConstants.PREFS_KEY_ANALYTICS)
//        didSave(preferences: preferences)
//    }
//
//    func getAnlyticsPrefs() -> Bool {
//        return UserDefaults.standard.bool(forKey: AConstants.PREFS_KEY_ANALYTICS)
//    }
    
    // Checking the UserDefaults is saved or not
    func didSave(preferences: UserDefaults) {
        let didSave = preferences.synchronize()
        if !didSave{
            L.log(message: "UserDefaults cannot be saved", event: .error)
        }
    }
    
}
