import Foundation


internal extension Request {
    func debugLog() -> Self {
        #if DEBUG
        debugPrint(self)
        #endif
        return self
    }
}


//TODO: get rid of tons of copypaste
class RESTUtil {
    open var apiHandler: APIHandler;
    
    static let instance = RESTUtil()
    
    private init() {
        self.apiHandler = ProdAPIHandler()
    }
    
    func updateApi(newApiType: APIHandler?) {
        if newApiType != nil {
            self.apiHandler = newApiType!
        } else {
            self.apiHandler = ProdAPIHandler()
        }
    }
    
    func getAppId(isHardcoded: Bool) -> String {
        let platform = Bundle.main.bundleIdentifier!
        
        //        if isHardcoded {
        //            platform = "com.ar.afw"
        //        }
        return platform
    }
    
    func addPlatformAndBundleId(endPoint: String) -> String {
        var result = endPoint
        let id = getAppId(isHardcoded: false)
        let platform = "ios"
        
        result = endPoint + platform + "/" + id
        return result;
    }
    
    func checkSDKVersionFailed() -> Bool {
        if !PrefsUtil.instance.isCurrentSDKVersionValid() {
            L.log(message: AConstants.getOutdatedSDKMessage(), event: .error)
            return true
        } else {
            // L.log(message: "BLU SDK version is valid. Current is: \(AConstants.CURRENT_BLU_SDK_VERSION) >= \(PrefsUtil.instance.getMinSDKInitVersion())", event: .info)
            return false
        }
    }
    
    func requestARInitData(emailUserSelf: String = "", onSuccess: @escaping ((_ jsData: ARInitData) -> Void), onError: @escaping ((_ message: String) -> Void)) {
        let endPoint = apiHandler.initAREndPoint();
        var result = endPoint
        let id = getAppId(isHardcoded: false)
        let platform = "ios"
        let version = ATools.instance.getAppVersionString()
        result = endPoint + platform + "/" + version + "/" + id
        
        L.log(message: "GET _\(String(describing: apiHandler))_ : \(result)", event: .info)
        
        var headers: HTTPHeaders = [
            "api-key": SwiftFromObjCBridge.sInst().bluLicenseKey,
            "sdk-version": "\(AConstants.CURRENT_BLU_SDK_VERSION)"
        ]
        
        if(!emailUserSelf.isEmpty){
             headers["email-user-self"] = emailUserSelf
        }
        
        SessionManager.default.request(result, headers: headers)
            // .debugLog()
            .responseJSON { response in
                guard response.result.error == nil else {
                    L.log(message: "error getting initialization data: \(response.result.error!)", event: .error)
                    onError("\(response.result.error!)")
                    return
                }
                
                if let data = response.data {
                    do {
                        let wikitudeInitResponse = try JSONDecoder().decode(ARInitResponse.self, from: data)
                        
                        if (wikitudeInitResponse.error == 0) {
                            if let rData = wikitudeInitResponse.data {
                                onSuccess(rData)
                            } else {
                                onError("")
                            }
                        } else if (wikitudeInitResponse.error == 7){
                            onError("Username not found")
                        } else {
                            if let rErrorMessage = wikitudeInitResponse.error_message {
                                onError(rErrorMessage)
                            } else {
                                onError("")
                            }
                        }
                    } catch {
                        L.log(message: "error parsing initialization data: \(error)", event: .error)
                        onError("")
                    }
                }
            }
            .responseString { response in
                if let error = response.result.error {
                    L.log(message: "initialization data error: \(error)", event: .error)
                }
                if let value = response.result.value {
                    L.log(message: "initialization data row: \(value)", event: .info)
                }
        }
    }
    
    func getListMarkerlessExperience(resultClosure: @escaping (_ resultData: [MarkerlessGroup]) -> Void) {
        if checkSDKVersionFailed() {
            resultClosure(Array<MarkerlessGroup>())
            return
        }
        
        let endPoint = addPlatformAndBundleId(endPoint: apiHandler.getListMarkerlessExperienceEndPoint());
        
        L.log(message: "GET _\(String(describing: apiHandler))_ : \(endPoint)", event: .info)
        
        SessionManager.default.request(endPoint, method: .get)
            .responseJSON { response in
                guard response.result.error == nil else {
                    L.log(message: "error getting list of markerless experiences: \(response.result.error!)", event: .error)
                    resultClosure(Array<MarkerlessGroup>())
                    return
                }
                guard response.result.isSuccess else {
                    L.log(message: "error getting list of markerless experiences: response is not successfull", event: .error)
                    resultClosure(Array<MarkerlessGroup>())
                    return
                }
                
                if let data = response.data {
                    do {
                        let groupsArray = try JSONDecoder().decode([MarkerlessGroup].self, from: data)
                        
                        resultClosure(groupsArray)
                    } catch {
                        
                        resultClosure(Array<MarkerlessGroup>())
                        L.log(message: "error parsing list of markerless experiences", event: .error)
                    }
                    
                }
            }
            .responseString { response in
                if let error = response.result.error {
                    L.log(message: "Get List ERROR: \(error)", event: .warning)
                }
                if let value = response.result.value {
                    L.log(message: "Get list string: \(value)", event: .info)
                }
        }
    }
    
    func getGroupsMarkerlessExperience(listId: Int, resultClosure: @escaping (_ resultData: [MarkerlessExperienceGroup]) -> Void) {
        if checkSDKVersionFailed() {
            resultClosure(Array<MarkerlessExperienceGroup>())
            return
        }
        
        var endPoint = addPlatformAndBundleId(endPoint: apiHandler.getGroupsMarkerlessExperienceEndPoint());
        endPoint = endPoint + "/" + String(listId)
        
        L.log(message: "GET _\(String(describing: apiHandler))_ : \(endPoint)", event: .info)
        
        SessionManager.default.request(endPoint, method: .get)
            .responseJSON { response in
                // check for errors
                guard response.result.error == nil else {
                    L.log(message: "error getting collection of markerless experiences: \(response.result.error!)", event: .error)
                    resultClosure(Array<MarkerlessExperienceGroup>())
                    return
                }
                guard response.result.isSuccess else {
                    L.log(message: "error getting collection of markerless experiences: response is not successfull", event: .error)
                    resultClosure(Array<MarkerlessExperienceGroup>())
                    return
                }
                
                if let data = response.data {
                    do {
                        //    let json = try JSONSerialization.jsonObject(with: data, options: [])
                        let groupsArray = try JSONDecoder().decode([MarkerlessExperienceGroup].self, from: data)
                        
                        resultClosure(groupsArray)
                    } catch let error {
                        resultClosure(Array<MarkerlessExperienceGroup>())
                        
                        L.log(message: "error parsing collection of markerless experiences: \(error.localizedDescription)", event: .error)
                    }
                    
                }
            }
            .responseString { response in
                if let error = response.result.error {
                    L.log(message: "Get collection ERROR: \(error)", event: .warning)
                }
                if let value = response.result.value {
                    L.log(message: "Get collection string: \(value)", event: .info)
                }
        }
        
    }
    
    func getMarkerlessById(experienceId: Int, onSuccess: @escaping ((MarkerlessExperience) -> Void), onError: @escaping ((String) -> Void)) {
        if checkSDKVersionFailed() {
            onError("")
            return
        }
        
        var endPoint = addPlatformAndBundleId(endPoint: apiHandler.getMarkerlessExperienceById());
        endPoint = endPoint + "/" + String(experienceId)
        
        L.log(message: "GET _\(String(describing: apiHandler))_ : \(endPoint)", event: .info)
        
        SessionManager.default.request(endPoint, method: .get)
            .responseJSON { response in
                // check for errors
                guard response.result.error == nil else {
                    L.log(message: "error getting markerless by Id: \(response.result.error!)", event: .error)
                    onError("")
                    return
                }
                guard response.result.isSuccess else {
                    L.log(message: "error getting markerless by Id: response is not successfull", event: .error)
                    onError("")
                    return
                }
                
                if let data = response.data {
                    do {
                        let subgroup = try JSONDecoder().decode([MarkerlessExperienceGroup].self, from: data)
                        
                        if let exp = subgroup.first?.experiences?.first {
                            onSuccess(exp)
                        } else {
                            onError("No such experience")
                            L.log(message: "error getting markerless by Id, no such experience", event: .error)
                        }
                    } catch {
                        onError("Experience doesn't exist")
                        
                        L.log(message: "error getting markerless by Id", event: .error)
                    }
                    
                }
            }
            .responseString { response in
                if let error = response.result.error {
                    L.log(message: "Get Markerless by Id ERROR: \(error)", event: .warning)
                }
                if let value = response.result.value {
                    L.log(message: "Get Markerless by Id string: \(value)", event: .info)
                }
        }
        
    }
    
    
    func getMarkerbasedTargets(resultClosure: @escaping (_ resultData: [MarkerbasedMarker]) -> Void) {
        if checkSDKVersionFailed() {
            resultClosure([MarkerbasedMarker]())
            return
        }
        
        let endPoint = addPlatformAndBundleId(endPoint: apiHandler.getMarkerbasedTargets());
        
        L.log(message: "GET _\(String(describing: apiHandler))_ : \(endPoint)", event: .info)
        
        SessionManager.default.request(endPoint, method: .get)
            .responseJSON { response in
                guard response.result.error == nil else {
                    L.log(message: "error getting markers list: \(response.result.error!))", event: .error)
                    resultClosure([MarkerbasedMarker]())
                    return
                }
                guard response.result.isSuccess else {
                    L.log(message: "error getting markers list: response is not successfull", event: .error)
                    resultClosure([MarkerbasedMarker]())
                    return
                }
                
                if let data = response.data {
                    do {
                        let dataArray = try JSONDecoder().decode([MarkerbasedMarker].self, from: data)
                        
                        resultClosure(dataArray)
                    } catch {
                        L.log(message: "error parsing markers list", event: .error)
                        
                        resultClosure([MarkerbasedMarker]())
                    }
                    
                }
            }
            .responseString { response in
                if let error = response.result.error {
                    L.log(message: "Get Markers ERROR: \(error)", event: .warning)
                }
                if let value = response.result.value {
                    L.log(message: "Get markers string: \(value)", event: .info)
                }
        }
    }
    
    func sendFeedback(name: String, email: String, phone: String, comment: String, os: String, appId: String, captcha: String,
                      onSuccess: @escaping (() -> Void), onError: @escaping (() -> Void)) {
        if checkSDKVersionFailed() {
            onError()
            return
        }
        
        let theEndpoint = apiHandler.sendFeedBackEndPoint();
        
        L.log(message: "POST _\(String(describing: apiHandler))_: \(theEndpoint)", event: .info)
        
        let appVersion = ATools.instance.getAppVersionString()
        
        let parametersDict: [String: Any] = [
            "request":
                [
                    "content":comment.trimmingCharacters(in: .whitespacesAndNewlines),
                    "email":email.trimmingCharacters(in: .whitespacesAndNewlines),
                    "name":name.trimmingCharacters(in: .whitespacesAndNewlines),
                    "phone":phone.trimmingCharacters(in: .whitespacesAndNewlines),
                    "os":os.trimmingCharacters(in: .whitespacesAndNewlines),
                    "appId":appId.trimmingCharacters(in: .whitespacesAndNewlines),
                    "recaptchaReactive": captcha.trimmingCharacters(in: .whitespacesAndNewlines),
                    "app_version": appVersion
            ]
        ]
        
        SessionManager.default.request(theEndpoint, method: .post, parameters: parametersDict, encoding: JSONEncoding.default)
            .responseJSON { response in
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    L.log(message: "POST feedback ERROR: \(response.result.error!)", event: .warning)
                    onError()
                    return
                }
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    onError()
                    return
                }
                
                if let message = json["message"] as? String {
                    if message == "success" { //TODO ask backend to send errorcode instead of checking message
                        onSuccess()
                    } else {
                        onError()
                    }
                } else {
                    onError()
                }
            }
            .responseString { response in
                if let error = response.result.error {
                    L.log(message: "POST Feedback ERROR: \(error)", event: .warning)
                }
                if let value = response.result.value {
                    L.log(message: "POST Feedback Response String Value: \(value)", event: .info)
                }
        }
    }
    
    func saveUserInfo(info: UserInfo, onSuccess: @escaping (() -> Void), onError: @escaping (() -> Void)) {
        if checkSDKVersionFailed() {
            onError()
            return
        }
        
        PrefsUtil.instance.saveUserPersonalInfo(userInfo: info)
        
        let theEndpoint = apiHandler.sendUserInfoEndPoint();
        
        L.log(message: "POST _\(String(describing: apiHandler))_: \(theEndpoint)", event: .info)
        
        let appVersion = ATools.instance.getAppVersionString()
        
        let parametersDict: [String: Any] = [
            "firstName":info.firstName,
            "lastName":info.secondName,
            "businessName":info.businessName,
            "email":info.email,
            "industry":info.industry,
            "music":self.getMusicJsonRepresentation(music: info.music),
            "gender":info.gender,
            "app_version": appVersion
        ]
        
        //        if let theJSONData = try? JSONSerialization.data( withJSONObject: parametersDict, options: .prettyPrinted),
        //            let theJSONText = String(data: theJSONData, encoding: String.Encoding.ascii) {
        //            L.log(message: "^^^ saveUserInfo   INPUT JSON string = \n\(theJSONText)", event: .warning)
        //        }
        
        SessionManager.default.request(theEndpoint, method: .post, parameters: parametersDict, encoding: JSONEncoding.default)
            .validate()
            .responseJSON { response in
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    L.log(message: "POST User Info ERROR: \(String(describing: response.result.error))", event: .warning)
                    onError()
                    return
                }
                guard response.result.isSuccess else {
                    L.log(message: "POST User Info ERROR: response is not successfull", event: .error)
                    onError()
                    return
                }
                // make sure we got some JSON since that's what we expect
                guard (response.result.value as? [String: Any]) != nil else {
                    onError()
                    return
                }
                onSuccess()
            }
            .responseString { response in
                if let error = response.result.error {
                    L.log(message: "POST User Info ERROR: \(error)", event: .warning)
                }
                if let value = response.result.value {
                    L.log(message: "POST User Info Response String Value: \(value)", event: .info)
                }
        }
    }
    
    func getUserPersonalVideo(expId: Int, storyboardId: Int, onSuccess: @escaping ((String) -> Void), onError: @escaping ((String) -> Void)) {
        if checkSDKVersionFailed() {
            onError("")
            return
        }
        
        let info = PrefsUtil.instance.getUserInfo()
        
        let theEndpoint = apiHandler.getUserPersonalVideo();
        
        L.log(message: "POST _\(String(describing: apiHandler))_: \(theEndpoint)", event: .info)
        
        let appVersion = ATools.instance.getAppVersionString()
        
        //        let parametersDict: [String: Any] =
        //            [
        //                "experience_id": expId,
        //                "storyboard_id": storyboardId,
        //                "user": [
        //                    "firstName":info.firstName,
        //                    "lastName":info.secondName,
        //                    "businessName":info.businessName,
        //                    "email":info.email,
        //                    "industry":info.industry,
        //                    "music":self.getMusicJsonRepresentation(music: info.music),
        //                    "gender":info.gender,
        //                    "app_version": appVersion
        //                ]
        //        ]
        //
        //        if let theJSONData = try? JSONSerialization.data( withJSONObject: parametersDict, options: .prettyPrinted),
        //            let theJSONText = String(data: theJSONData, encoding: String.Encoding.ascii) {
        //            L.log(message: "^^^ getUserPersonalVideo   INPUT JSON string = \n\(theJSONText)", event: .warning)
        //        }
        
        var request = URLRequest(url: URL(string: theEndpoint)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let uglyJson = "{\"storyboard_id\":\(storyboardId),\"experience_id\":\(expId),\"user\":{\"gender\":\"\(info.gender)\",\"app_version\":\"\(appVersion)\",\"businessName\":\"\(info.businessName)\",\"firstName\":\"\(info.firstName)\",\"email\":\"\(info.email)\",\"lastName\":\"\(info.secondName)\",\"industry\":\"\(info.industry)\",\"music\":\"\(info.music)\"}}"
        
        let uglyData = uglyJson.data(using: String.Encoding.utf8, allowLossyConversion: false)!
        request.httpBody = uglyData
        
        SessionManager.default.request(request)
            .responseJSON { response in
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    L.log(message: "POST User Personal Video ERROR: \(response.result.error!)", event: .warning)
                    onError("")
                    return
                }
                if (response.result.value as? [String: Any]) != nil {
                    //                    let aaa = NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue)
                    //                    print("^^^^  \(aaa)")
                    onSuccess("") //if user didn't save personal info and server doesn't know about him, it will return "{}" - empty JSON, this is ok, we just will not show personal video to this user.
                    return
                }
                
                guard let videoUrl = response.result.value as? String else {
                    onError("response value is not String")
                    return
                }
                //                let aaa = NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue)
                //                print("^^^^  \(aaa)")
                onSuccess(videoUrl)
            }
            .responseString { response in
                if let error = response.result.error {
                    L.log(message: "POST User Personal Video ERROR: \(error)", event: .warning)
                }
                if let value = response.result.value {
                    L.log(message: "POST User Personal Video Response String Value: \(value)", event: .info)
                }
        }
        
    }
    
    private func getMusicJsonRepresentation(music: String) -> String {
        var jsonMusic = ""
        switch music {
        case UserMusic.Alternative.string():
            jsonMusic = "alternative"
            
        case UserMusic.Classical.string():
            jsonMusic = "classical"
            
        case UserMusic.ClassicRock.string():
            jsonMusic = "classicrock"
            
        case UserMusic.Country.string():
            jsonMusic = "country"
            
        case UserMusic.Jazz.string():
            jsonMusic = "jazz"
            
        case UserMusic.RnB.string():
            jsonMusic = "randb"
        default:
            jsonMusic = "jazz"
        }
        return jsonMusic
        
    }
    
    func login(userName: String, password: String,  onResult: @escaping (_ success: Bool) -> Void) {
        if checkSDKVersionFailed() {
            onResult(false)
            return
        }
        
        let endPoint = apiHandler.endUserLoginEndPoint();
        
        L.log(message: "POST _\(String(describing: apiHandler))_ : \(endPoint)", event: .info)
        
        SessionManager.default.request(endPoint, method: .post, parameters:  [
            "user":userName, "password":password
            ], encoding: JSONEncoding.default)
            .responseJSON { response in
                guard response.result.error == nil else {
                    L.log(message: "POST login ERROR: \(response.result.error!)", event: .warning)
                    onResult(false)
                    return
                }
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any], let message = json["message"] as? String else {
                    onResult(false)
                    return
                }
                onResult(message == "valid")
            }
            .responseString { response in
                if let error = response.result.error {
                    L.log(message: "POST login ERROR: \(error)", event: .warning)
                }
                if let value = response.result.value {
                    L.log(message: "POST login Response String Value: \(value)", event: .info)
                }
        }
    }
    
    func resetPassword(email: String, onResult: @escaping (_ success: Bool) -> Void) {
        if checkSDKVersionFailed() {
            onResult(false)
            return
        }
        
        let endPoint = apiHandler.endUserEmailPass();
        
        L.log(message: "POST _\(String(describing: apiHandler))_ : \(endPoint)", event: .info)
        
        SessionManager.default.request(endPoint, method: .post, parameters:  ["email":email], encoding: JSONEncoding.default)
            .responseJSON { response in
                guard response.result.error == nil else {
                    L.log(message: "POST resetPassword ERROR: \(response.result.error!)", event: .warning)
                    onResult(false)
                    return
                }
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any], let message = json["message"] as? String else {
                    onResult(false)
                    return
                }
                onResult(message == "success")
            }
            .responseString { response in
                if let error = response.result.error {
                    L.log(message: "POST resetPassword ERROR: \(error)", event: .warning)
                }
                if let value = response.result.value {
                    L.log(message: "POST resetPassword Response String Value: \(value)", event: .info)
                }
        }
    }
    
}

