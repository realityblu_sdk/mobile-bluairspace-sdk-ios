import Foundation


@objc class WikitudeUtil: NSObject {
    
    @objc class var swiftSharedInstance: WikitudeUtil {
        struct Singleton {
            static let instance = WikitudeUtil()
        }
        return Singleton.instance
    }
    
    // the sharedInstance class method can be reached from ObjC
    @objc class func sharedInstance() -> WikitudeUtil {
        return WikitudeUtil.swiftSharedInstance
    }
    
    
    var onArchitectWorldFolderMovedToDocuments: (() -> Void)? //method to be called in some viewcontroller
    
    func moveArchitectWorldFolderToDocuments(onMoved: @escaping (() -> Void), onErrorMoving: @escaping (() -> Void)) {
        if let wuFolderUrl = Bundle.main.url(forResource: "wikitude", withExtension: nil) {
            // print("\(TAG) moveArchitectWorldFolderToDocuments assets wikitude url: \(wuFolderUrl), path: \(wuFolderUrl.path)")

            let fileManager = FileManager.default
            let doumentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
            let destinationPath = doumentDirectoryPath.appendingPathComponent("wikitude")

            // print("\(TAG) moveArchitectWorldFolderToDocuments destination wikitude path: \(destinationPath)")

            if !fileManager.fileExists(atPath: destinationPath) {
                do {
                    try fileManager.copyItem(atPath: wuFolderUrl.path, toPath: destinationPath)
                    onMoved()
                } catch {
                    //  print("\(self.TAG) onArchitectWorldFolderMovedToDocuments ERROR copy: \(error)")
                    onErrorMoving()
                }
            } else {
                do {
                    // print("\(TAG) moveArchitectWorldFolderToDocuments - FILE EXISTS, REMOVING IT: \(destinationPath)")
                    try fileManager.removeItem(atPath: destinationPath)
                    try fileManager.copyItem(atPath: wuFolderUrl.path, toPath: destinationPath)
                    onMoved()
                } catch {
                    //  print("\(self.TAG) moveArchitectWorldFolderToDocuments ERROR remove/copy: \(error)")
                    onErrorMoving()
                }
            }
        }
    }

    func getIconPlaceholderAssetPath() -> String {
        if let path = Bundle.main.url(forResource: "wikitude/02_InstantTracking/assets/ar_placeholder_white", withExtension: "png") {
            return path.path
        }
        return ""
    }
    
    func getWT3PlaceholderAssetPath() -> String {
        if let path = Bundle.main.url(forResource: "wikitude/02_InstantTracking/assets/models/placeholder", withExtension: "wt3") {
            return path.path
        }
        return ""
    }
    
//    func downloadAndReplaceMarkerBasedInitFile(jsUrl: String, onSuccess: @escaping (() -> Void), onError: @escaping (() -> Void)) {
//        tryDownloadTempFileAndCheckSize(jsUrl: jsUrl, onSuccess: {
//            self.tryDownloadAndReplaceRealMarkerBasedInitFile(jsUrl: jsUrl, onSuccess: onSuccess, onError: onError)
//        }) {
//            onError()
//        }
//    }
    
//    func tryDownloadAndReplaceRealMarkerBasedInitFile(jsUrl: String, onSuccess: @escaping (() -> Void), onError: @escaping (() -> Void)) {
//        let fileName = jsUrl.components(separatedBy: "/").last!;
//
//        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
//            let documentsURL:NSURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
//            let fileURL = documentsURL.appendingPathComponent("ArchitectWorld/CloudReco/js/\(fileName)")
//            return (fileURL!, [.removePreviousFile, .createIntermediateDirectories])
//        }
//
//        Alamofire.download(jsUrl, to: destination).downloadProgress(closure: { (prog) in
//        }).response { response in
//            if response.error == nil {
//                //   print("download REAL MarkerBased Init File SUCCESS, file saved in: \(filePath)")
//                onSuccess()
//
//            } else {
//                //  print("\(self.TAG) download REAL MarkerBased Init File ERROR loading file: \(response.error.debugDescription)")
//                onError()
//            }
//        }
//    }
    
    //MARK: FIXME copypaste tryDownloadAndReplaceRealMarkerBasedInitFile
//    func tryDownloadTempFileAndCheckSize(jsUrl: String, onSuccess: @escaping (() -> Void), onError: @escaping (() -> Void)) {
//        let realFileName = jsUrl.components(separatedBy: "/").last!
//        let fileName = "test_" + realFileName
//
//        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
//            let documentsURL:NSURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
//            let fileURL = documentsURL.appendingPathComponent("ArchitectWorld/CloudReco/js/\(fileName)")
//            return (fileURL!, [.removePreviousFile, .createIntermediateDirectories])
//        }
//
//        Alamofire.download(jsUrl, to: destination).downloadProgress(closure: { (prog) in
//        }).response { response in
//            if response.error == nil, let filePath = response.destinationURL?.path {
//                let size = self.checkFileSize(filePath: filePath)
//
//                if size > 20000 { //27331 for example
//                    //       print("download TEST MarkerBased Init File SUCCESS, file saved in: \(filePath), file size: \(size)")
//                    onSuccess()
//                } else {
//                    //       print("download TEST MarkerBased Init File ERROR, too small, file saved in: \(filePath), file size: \(size)")
//                    onError()
//                }
//            } else {
//                //   print("download TEST MarkerBased Init File ERROR loading file")
//
//                onError()
//            }
//        }
//    }
//
//    func checkFileSize(filePath: String) -> UInt64 {
//        var fileSize : UInt64
//
//        do {
//            //return [FileAttributeKey : Any]
//            let attr = try FileManager.default.attributesOfItem(atPath: filePath)
//            fileSize = attr[FileAttributeKey.size] as! UInt64
//
//            //if you convert to NSDictionary, you can get file size old way as well.
//            let dict = attr as NSDictionary
//            fileSize = dict.fileSize()
//
//            //   print("checkFileSize: \(fileSize)")
//
//            return fileSize
//        } catch {
//            //  print("Error: \(error)")
//        }
//
//        return 99999;
//    }
    
    
    
}
