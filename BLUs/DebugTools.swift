import Foundation
import FirebaseCore

public class DebugTools: NSObject {
    public static func setEnvironmentType(environmentType: APIHandler?) {
        BLUHelper.instance().switchEnvironment(environmentType: environmentType)
    }

    public static func mock() {
        BLUHelper.instance().mock()
    }
    
    public static func unMock() {
        BLUHelper.instance().unMock()
    }
    
    public static func enableLogging(enable: Bool) {
        L.enableLogging(enable: enable)
    }
    
    public static func enableAnalytics(enable: Bool) {
        AnalyticsConfiguration.shared().setAnalyticsCollectionEnabled(enable)
    }
    
    public static func setEnvironment(EnvironmentName: String){
       AnalyticsUtil.setEnvironment(EnvironmentName: EnvironmentName)
    }
    
    public static func disableTutorial() {
        PrefsUtil.instance.setNeedMarkerBasedTutorial(value: false)
    }
}
