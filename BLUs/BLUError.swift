//
//  BLUError.swift
//  BLUs
//
//  Created by jegor on 10/3/19.
//  Copyright © 2019 Den Alex. All rights reserved.
//

import Foundation

public enum BLUErrorType {
    case warning, error
}

public protocol BLUError {
    var type: BLUErrorType { get }
    var description: String { get }
}


internal struct BLUBasicError: BLUError {
    public var type: BLUErrorType
    public var description: String
    
    init(_ type: BLUErrorType, description: String) {
        self.type = type
        self.description = description
    }
}
