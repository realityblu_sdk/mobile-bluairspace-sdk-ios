## Framework distribution

To deliver framework you have to use `BLUs-Universal` target, which is running `Append missing architectures` build phase, so end user developers could use framework on simulator as well. Result of the Archive operation on this target (so called fat binary), you can find inside `blu-universal` folder in the the project root.

Conversely, before App Store submission, you have to remove unsupported archetypes (simulator x86_64 & i386) from frameworks on build. To perform this, follow the instructions in [App Store submission](#app-store-submission) section